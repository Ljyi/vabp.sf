﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace VAbp.SF.EntityFrameworkCore
{
    [DependsOn(
        typeof(SFEntityFrameworkCoreModule)
        )]
    public class SFEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<SFMigrationsDbContext>();
        }
    }
}
