﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using VAbp.SF.Data;
using Volo.Abp.DependencyInjection;

namespace VAbp.SF.EntityFrameworkCore
{
    public class EntityFrameworkCoreSFDbSchemaMigrator
        : ISFDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreSFDbSchemaMigrator(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the SFMigrationsDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<SFMigrationsDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}