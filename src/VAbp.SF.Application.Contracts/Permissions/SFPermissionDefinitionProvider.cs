﻿using VAbp.SF.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace VAbp.SF.Permissions
{
    public class SFPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(SFPermissions.GroupName);

            //Define your own permissions here. Example:
            //myGroup.AddPermission(SFPermissions.MyPermission1, L("Permission:MyPermission1"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<SFResource>(name);
        }
    }
}
