﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace VAbp.SF
{
    /// <summary>
    /// 添加
    /// </summary>
    public class BaseAddDto
    {
        /// <summary>
        /// 是否删除
        /// </summary>
        [JsonIgnoreAttribute]
        public bool IsDelete { get { return false; } }
        /// <summary>
        /// 创建人
        /// </summary>
        [JsonIgnoreAttribute]
        public string CreateUser { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [JsonIgnoreAttribute]
        public DateTime? CreateTime { get; set; }
    }
}
