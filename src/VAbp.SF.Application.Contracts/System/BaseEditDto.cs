﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace VAbp.SF.System
{
    /// <summary>
    /// 编辑
    /// </summary>
    public class BaseEditDto
    {
        /// <summary>
        /// 自增Id
        /// </summary>
        public virtual int Id { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        [JsonIgnoreAttribute]
        public string UpdateUser { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [JsonIgnoreAttribute]
        public DateTime? UpdateTime { get; set; }
    }
}
