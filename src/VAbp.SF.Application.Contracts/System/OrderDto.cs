﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF
{
    public class OrderInfo
    {
        public int Id { get; set; }
        public string OrderNo { get; set; }
        public int Channel { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        public string Remark { get; set; }
        public string Phone { get; set; }
        public int Status { get; set; }
        public bool IsDelete { get; set; }
        public string CreateUser { get; set; }
        public DateTime CredateTime { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateTime { get; set; }
    }
    public class OrderDto
    {
        public int Id { get; set; }
        public string OrderNo { get; set; }
        public int Channel { get; set; }
        public string ChannelName { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        public string Remark { get; set; }
        public string Phone { get; set; }
        public int Status { get; set; }
        public bool IsDelete { get; set; }
        public string CreateUser { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime CredateTime { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateTime { get; set; }
    }
    public class AddOrderDto
    {
        public int Channel { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        public string Remark { get; set; }
        public string Phone { get; set; }
    }

    /// <summary>
    /// 订单查询
    /// </summary>
    public class OrderCount
    {
        /// <summary>
        /// 全部、今日
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 订单数量
        /// </summary>
        public int Count { get; set; }
    }
    /// <summary>
    /// 订单渠道
    /// </summary>
    public class OrderChannel
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 渠道名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 订单数量
        /// </summary>
        public int Count { get; set; }
    }
}
