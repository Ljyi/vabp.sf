﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VAbp.SF.System
{
    /// <summary>
      /// 菜单
      /// </summary>
    public class AddMenuDto : BaseAddDto
    {
        /// <summary>
        /// 菜单名称
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string MenuName { get; set; }
        /// <summary>
        /// <summary>
        /// Url
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Url { get; set; }
        /// 菜单级别
        /// </summary>
        [Required]
        public int MenuLevel { get; set; }
        /// <summary>
        /// 父节点Id
        /// </summary>
        [Required]
        public int ParentId { get; set; }
        /// <summary>
        /// 页面别名
        /// </summary>
        [MaxLength(200)]
        [Required]
        public string Alias { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// Icon
        /// </summary>
        [MaxLength(100)]
        public string Icon { get; set; }
        /// <summary>
        /// 描述信息
        /// </summary>
        [MaxLength(200)]
        public string Description { get; set; }
        /// <summary>
        /// 状态 1：正常，0：禁用
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 前端组件(.vue)
        /// </summary>
        [MaxLength(200)]
        public string Component { get; set; }
        /// <summary>
        /// 是否隐藏
        /// </summary>
        public bool HideInMenu { get; set; }
        /// <summary>
        /// 是否展示
        /// </summary>
        public bool IsShow { get; set; }
        /// <summary>
        /// 缓存
        /// </summary>
        public bool NotCache { get; set; }
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public class EditMenuDto : BaseEditDto
    {
        /// <summary>
        /// 菜单名称
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string MenuName { get; set; }
        /// <summary>
        /// Url
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Url { get; set; }
        /// <summary>
        /// 页面别名
        /// </summary>
        [MaxLength(200)]
        public string Alias { get; set; }
        /// <summary>
        /// 菜单级别
        /// </summary>
        [Required]
        public int MenuLevel { get; set; }
        /// <summary>
        /// 父节点Id
        /// </summary>
        [Required]
        public int ParentId { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// Icon
        /// </summary>
        [MaxLength(100)]
        public string Icon { get; set; }
        /// <summary>
        /// 描述信息
        /// </summary>
        [MaxLength(200)]
        public string Description { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 前端组件(.vue)
        /// </summary>
        [MaxLength(200)]
        public string Component { get; set; }
        /// <summary>
        /// 是否隐藏
        /// </summary>
        public bool HideInMenu { get; set; }
        /// <summary>
        /// 是否展示
        /// </summary>
        public bool IsShow { get; set; }
        /// <summary>
        /// 缓存
        /// </summary>
        public bool NotCache { get; set; }
    }

    /// <summary>
    /// 菜单
    /// </summary>
    public class MenuInfo
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 菜单编码
        /// </summary>
        public string MenuCode { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// Url
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 菜单级别
        /// </summary>
        public int MenuLevel { get; set; }
        /// <summary>
        /// 页面别名
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// 父节点Id
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 父节点
        /// </summary>
        public List<int> ParentList { get; set; }
        /// <summary>
        /// 父级菜单名称
        /// </summary>
        public string ParentName { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// Icon
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 描述信息
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 前端组件(.vue)
        /// </summary>
        public string Component { get; set; }
        /// <summary>
        /// 是否默认路由
        /// </summary>
        public int IsDefaultRouter { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUser { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string UpdateUser { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public string UpdateTime { get; set; }
        /// <summary>
        /// 是否隐藏
        /// </summary>
        public bool HideInMenu { get; set; }
        /// <summary>
        /// 是否展示
        /// </summary>
        public bool IsShow { get; set; }
        /// <summary>
        /// 缓存
        /// </summary>
        public bool NotCache { get; set; }
    }
}
