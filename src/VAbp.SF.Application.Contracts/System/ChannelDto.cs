﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VAbp.SF.System
{

    public class AddChannelDto : BaseAddDto
    {
        /// <summary>
        /// 渠道编码
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string ChannelCode { get; set; }
        /// <summary>
        /// 渠道名称
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string ChannelName { get; set; }
        /// <summary>
        /// 父节点Id
        /// </summary>
        [Required]
        public int ParentId { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 是否展示
        /// </summary>
        public bool Enable { get; set; }
    }
    public class EditChannelDto : BaseEditDto
    {
        /// <summary>
        /// 渠道编码
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string ChannelCode { get; set; }
        /// <summary>
        /// 渠道名称
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string ChannelName { get; set; }
        /// <summary>
        /// 父节点Id
        /// </summary>
        [Required]
        public int ParentId { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 是否展示
        /// </summary>
        public bool Enable { get; set; }
    }
    public class ChannelInfo
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 渠道编码
        /// </summary>
        public string ChannelCode { get; set; }
        /// <summary>
        /// 渠道名称
        /// </summary>
        public string ChannelName { get; set; }
        /// <summary>
        /// 父节点Id
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 是否展示
        /// </summary>
        public bool Enable { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUser { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CredateTime { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string UpdateUser { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    }
    public class ChannelDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 渠道编码
        /// </summary>
        public string ChannelCode { get; set; }
        /// <summary>
        /// 渠道名称
        /// </summary>
        public string ChannelName { get; set; }
        /// <summary>
        /// 父节点Id
        /// </summary>
        public List<int> ParentId { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 是否展示
        /// </summary>
        public bool Enable { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUser { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CredateTime { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string UpdateUser { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    }
    /// <summary>
    /// 渠道
    /// </summary>
    public class ChannelTree
    {
        public ChannelTree()
        {
            ChannelChildTrees = new List<ChannelTree>();
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 渠道名称
        /// </summary>
        public string ChannelName { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 子节点
        /// </summary>
        public List<ChannelTree> ChannelChildTrees { get; set; }
    }
    /// <summary>
    /// 子节点
    /// </summary>
    public class ChannelChildTree
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 渠道名称
        /// </summary>
        public string ChannelName { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
