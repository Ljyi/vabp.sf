﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.System
{
    public class OrderExportModel
    {
        public string OrderNo { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        public string Remark { get; set; }
        public string Phone { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public string Date { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        public string Time { get; set; }
    }
}
