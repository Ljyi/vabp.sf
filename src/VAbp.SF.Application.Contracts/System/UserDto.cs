﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace VAbp.SF.System
{
    public class AddUserDto
    {
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 登录账号
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [JsonIgnoreAttribute]
        public string Status { get; set; }
        /// <summary>
        /// 是否是管理员
        /// </summary>
        [JsonIgnoreAttribute]
        public bool IsAdmin { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        [JsonIgnoreAttribute]
        public bool IsDelete { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        [JsonIgnoreAttribute]
        public string CreateUser { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [JsonIgnoreAttribute]
        public DateTime CredateTime { get; set; }

        public AddUserDto()
        {
            CredateTime = DateTime.Now;
            IsDelete = false;
            IsAdmin = false;
            Status = "";
        }
    }
    public class UserDto: EntityDto<int>
    {
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 登录账号
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 头像地址
        /// </summary>
        [MaxLength(100)]
        public string HeadImg { get; set; }
        /// <summary>
        /// 是否锁定
        /// </summary>
        public bool IsLocked { get; set; }
        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool Forbidden { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 是否是管理员
        /// </summary>
        public bool IsAdmin { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDelete { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUser { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CredateTime { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string UpdateUser { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    }

    /// <summary>
    /// 编辑用户
    /// </summary>
    public class UpdateUserDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string UserName { get; set; }
        /// <summary>
        /// 头像地址
        /// </summary>
        [MaxLength(100)]
        public string HeadImg { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        [MaxLength(100)]
        public string Email { get; set; }
        /// <summary>
        /// 旧密码
        /// </summary>
        [MaxLength(34)]
        public string Password { get; set; }
        /// <summary>
        /// 新密码
        /// </summary>
        [MaxLength(34)]
        public string NewPassword { get; set; }
        /// <summary>
        /// 创建人（前端不用填写）
        /// </summary>
        [MaxLength(50)]
        public string UpdateUser { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    }
    /// <summary>
    /// 编辑用户
    /// </summary>
    public class ModifyUserDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string UserName { get; set; }
        /// <summary>
        /// 头像地址
        /// </summary>
        [MaxLength(100)]
        public string HeadImg { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        [MaxLength(100)]
        public string Email { get; set; }
        /// <summary>
        /// 角色Id
        /// </summary>
        public int RoleId { get; set; }
        /// <summary>
        /// 创建人（前端不用填写）
        /// </summary>
        [MaxLength(50)]
        public string UpdateUser { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    } 
    /// <summary>
      /// 用户信息
      /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 登录账号
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 头像地址
        /// </summary>
        public string HeadImg { get; set; }
        /// <summary>
        /// 是否锁定
        /// </summary>
        public bool IsLocked { get; set; }
        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool Forbidden { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 是否是管理员
        /// </summary>
        public bool IsAdmin { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDelete { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUser { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CredateTime { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string UpdateUser { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    }
}
