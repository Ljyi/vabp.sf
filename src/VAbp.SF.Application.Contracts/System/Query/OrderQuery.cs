﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.System.Query
{
    public class OrderQuery : PageParameter
    {
        public OrderQuery()
        {
            Channel = new List<int>();
        }
        /// <summary>
        /// 渠道
        /// </summary>
        public List<int> Channel { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public string StartDatetime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndDatetime { get; set; }
    }
}
