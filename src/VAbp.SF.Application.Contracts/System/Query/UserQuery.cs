﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.System.Query
{
    public class UserQuery : PageParameter
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keywords { get; set; }
    }
}
