﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.System.Query
{
    public class MenuIconQuery : PageParameter
    {
        /// <summary>
        /// Iocn  code
        /// </summary>
        public string Code { get; set; }
    }
}
