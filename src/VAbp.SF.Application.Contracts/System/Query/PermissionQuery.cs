﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.System.Query
{
    public class PermissionQuery : PageParameter
    {
        /// <summary>
        /// 菜单Id
        /// </summary>
        public int? MenuId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string key { get; set; }
    }
}
