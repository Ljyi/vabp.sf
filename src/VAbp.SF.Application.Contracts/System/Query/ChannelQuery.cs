﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.System.Query
{
    public class ChannelQuery : PageParameter
    {
        /// <summary>
        ///
        /// </summary>
        public string Name { get; set; }

        public int? ParentId { get; set; }
    }
}
