﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.System.Query
{
    public class MenuQuery : PageParameter
    {
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 上级菜单GUID
        /// </summary>
        public int? ParentId { get; set; }
        /// <summary>
        /// 搜索关键字
        /// </summary>
        public string Kw { get; set; }
    }
}