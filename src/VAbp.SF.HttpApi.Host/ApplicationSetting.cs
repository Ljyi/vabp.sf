﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VAbp.SF
{
    public class ApplicationSetting
    {
        private const string Prefix = "Application";

        public const int MaxPageSize = 100;
        /// <summary>
        /// Token 过期时间
        /// </summary>
        public const int TokenExpires = 24 * 60 * 60 * 100;//24小时
        /// <summary>
        /// Jwt Token允许刷新天数
        /// </summary>
        public const int JwtTokenRefreshDay = 15;
    }
}
