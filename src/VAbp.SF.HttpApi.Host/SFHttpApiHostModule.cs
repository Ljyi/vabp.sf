using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using VAbp.SF.EntityFrameworkCore;
using VAbp.SF.MultiTenancy;
using StackExchange.Redis;
using Microsoft.OpenApi.Models;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.MultiTenancy;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared;
using Volo.Abp.AspNetCore.Serilog;
using Volo.Abp.Autofac;
using Volo.Abp.Caching;
using Volo.Abp.Caching.StackExchangeRedis;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;
using System.Configuration;
using VAbp.SF.Auth;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using VAbp.SF.Filter;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using VAbp.SF.Auth.Policys;
using System.Collections.Generic;
using System.Text;
using VAbp.SF.Base;
using VAbp.SF.BackgroundJobs;
using Microsoft.AspNetCore.Http;

namespace VAbp.SF
{
    [DependsOn(
        typeof(SFHttpApiModule),
        typeof(AbpAutofacModule),
        typeof(AbpCachingStackExchangeRedisModule),
        typeof(AbpAspNetCoreMvcUiMultiTenancyModule),
        typeof(SFApplicationModule),
        typeof(SFEntityFrameworkCoreDbMigrationsModule),
        typeof(BackgroundJobsModule),
        typeof(AbpAspNetCoreSerilogModule)
        )]
    public class SFHttpApiHostModule : AbpModule
    {
        private const string DefaultCorsPolicyName = "Default";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var configuration = context.Services.GetConfiguration();
            var hostingEnvironment = context.Services.GetHostingEnvironment();

            ConfigureConventionalControllers();
            //  ConfigureAuthentication(context, configuration);
            // ConfigureLocalization();
            ConfigureCache(configuration);
            ConfigureVirtualFileSystem(context);
         //   ConfigureRedis(context, configuration, hostingEnvironment);
            ConfigureCors(context, configuration);
            ConfigureJwtService(context, configuration);
            ConfigureSwaggerServices(context);
        }

        private void ConfigureCache(IConfiguration configuration)
        {
            Configure<AbpDistributedCacheOptions>(options =>
            {
                options.KeyPrefix = "SF:";
            });
        }

        private void ConfigureVirtualFileSystem(ServiceConfigurationContext context)
        {
            var hostingEnvironment = context.Services.GetHostingEnvironment();

            if (hostingEnvironment.IsDevelopment())
            {
                Configure<AbpVirtualFileSystemOptions>(options =>
                {
                    options.FileSets.ReplaceEmbeddedByPhysical<SFDomainSharedModule>(Path.Combine(hostingEnvironment.ContentRootPath, $"..{Path.DirectorySeparatorChar}VAbp.SF.Domain.Shared"));
                    options.FileSets.ReplaceEmbeddedByPhysical<SFDomainModule>(Path.Combine(hostingEnvironment.ContentRootPath, $"..{Path.DirectorySeparatorChar}VAbp.SF.Domain"));
                    options.FileSets.ReplaceEmbeddedByPhysical<SFApplicationContractsModule>(Path.Combine(hostingEnvironment.ContentRootPath, $"..{Path.DirectorySeparatorChar}VAbp.SF.Application.Contracts"));
                    options.FileSets.ReplaceEmbeddedByPhysical<SFApplicationModule>(Path.Combine(hostingEnvironment.ContentRootPath, $"..{Path.DirectorySeparatorChar}VAbp.SF.Application"));
                });
            }
        }

        private void ConfigureConventionalControllers()
        {
            Configure<AbpAspNetCoreMvcOptions>(options =>
            {
                options.ConventionalControllers.Create(typeof(SFApplicationModule).Assembly);
            });
        }

        private void ConfigureAuthentication(ServiceConfigurationContext context, IConfiguration configuration)
        {
            context.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = configuration["AuthServer:Authority"];
                    options.RequireHttpsMetadata = true;
                    options.Audience = "SF";
                });
        }
        /// <summary>
        /// services
        /// </summary>
        /// <param name="context"></param>
        private void ConfigureJwtService(ServiceConfigurationContext context, IConfiguration configuration)
        {
            //读取配置文件
            var JwtSetting = configuration.GetSection($"{nameof(ConfigSettings)}:{nameof(ConfigSettings.JwtSetting)}");
            var Issuer = JwtSetting[$"{ nameof(ConfigSettings.JwtSetting.Issuer)}"];
            var Audience = JwtSetting[$"{nameof(ConfigSettings.JwtSetting.Audience)}"];
            var symmetricKeyAsBase64 = JwtSetting[$"{ nameof(ConfigSettings.JwtSetting.SecurityKey)}"];
            var keyByteArray = Encoding.ASCII.GetBytes(symmetricKeyAsBase64);
            var signingKey = new SymmetricSecurityKey(keyByteArray);
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            var expires = double.Parse(JwtSetting[$"{ nameof(ConfigSettings.JwtSetting.Expires)}"]);
            //double.Parse(AppSettingConfigHelper.GetSectionValue(nameof(ConfigSettings.JwtSetting.Expires)));
            // 令牌验证参数，之前我们都是写在AddJwtBearer里的
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,//验证发行人的签名密钥
                IssuerSigningKey = signingKey,
                ValidateIssuer = true,//验证发行人
                ValidIssuer = Issuer,//发行人
                ValidateAudience = true,//验证订阅人
                ValidAudience = Audience,//订阅人
                ValidateLifetime = true,//验证生命周期
                ClockSkew = TimeSpan.Zero,//这个是定义的过期的缓存时间
                RequireExpirationTime = true,//是否要求过期
            };
            //这个集合模拟用户权限表,可从数据库中查询出来
            var permission = new List<Permission>
            {
            };

            var permissionRequirement = new PermissionRequirementM(
                "/api/appuser/denied",// 拒绝授权的跳转地址
                permission,
                ClaimTypes.Sid,
                ClaimTypes.Role,//基于角色的授权
                Issuer,//发行人
                Audience,//订阅人
                signingCredentials,//签名凭据
                expiration: TimeSpan.FromSeconds(expires),//接口的过期时间 Token过期时间ApplicationSetting.TokenExpires
                PermissionName: ""
                );
            // 自定义基于策略的授权权限
            context.Services.AddAuthorization(options =>
            {
                options.AddPolicy("Permission",
                         policy => policy.Requirements.Add(permissionRequirement));
            })
            .AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = nameof(ApiAuthenticationHandler);
                options.DefaultForbidScheme = nameof(ApiAuthenticationHandler);
            })
            .AddJwtBearer(o =>
            {
                //不使用https
                o.RequireHttpsMetadata = false;
                o.TokenValidationParameters = tokenValidationParameters; o.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        // 如果过期，则把<是否过期>添加到，返回头信息中
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                            //context.Response.StatusCode = 200;
                            //context.Response.Redirect("/api/appuser/denied");
                        }
                        return Task.CompletedTask;
                    }
                };
            }).AddScheme<AuthenticationSchemeOptions, ApiAuthenticationHandler>(nameof(ApiAuthenticationHandler), o => { });
            //注入授权Handler
            context.Services.AddSingleton<IAuthorizationHandler, PermissionHandler>();
            context.Services.AddSingleton(permissionRequirement);
            context.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        private void ConfigureSwaggerServices(ServiceConfigurationContext context)
        {
            var basePath = AppContext.BaseDirectory;
            context.Services.AddSwaggerGen(
                options =>
                {
                    options.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Title = "SF API 接口文档",
                        Version = "v1",
                        Description = "v1 接口文档说明 ",
                    });
                    options.DocInclusionPredicate((docName, description) => true);
                    //options.DocInclusionPredicate((docName, description) =>
                    //{
                    //    if (description.GroupName != null && (description.GroupName.StartsWith("Identity") ||
                    //        description.GroupName.StartsWith("Abp") || description.GroupName.StartsWith("Features") ||
                    //        description.GroupName == "Profile") || description.GroupName.StartsWith("Tenant"))
                    //    {
                    //        return false;
                    //    }
                    //    else
                    //    {
                    //        return true;
                    //    }
                    //});

                    //  options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Resources/Meowv.Blog.HttpApi.xml"));
                    //  options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Resources/Meowv.Blog.Domain.xml"));
                    //   options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Resources/Meowv.Blog.Application.Contracts.xml"));

                    //options.OperationFilter<AddResponseHeadersFilter>();
                    //options.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
                    //options.OperationFilter<SecurityRequirementsOperationFilter>();
                    #region 小绿锁，JWT身份认证配置
                    var security = new OpenApiSecurityScheme
                    {
                        Description = "JWT模式授权，请输入 Bearer {Token} 进行身份验证",
                        Name = "Authorization",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.ApiKey
                    };
                    options.AddSecurityDefinition("oauth2", security);
                    options.AddSecurityRequirement(new OpenApiSecurityRequirement { { security, new List<string>() } });
                    #endregion
                    // 应用Controller的API文档描述信息
                    // options.DocumentFilter<SwaggerDocumentFilter>();
                });

        }

        private void ConfigureRedis(ServiceConfigurationContext context, IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            if (!hostingEnvironment.IsDevelopment())
            {
                var redis = ConnectionMultiplexer.Connect(configuration["Redis:Configuration"]);
                context.Services
                    .AddDataProtection()
                    .PersistKeysToStackExchangeRedis(redis, "SF-Protection-Keys");
            }
        }

        private void ConfigureCors(ServiceConfigurationContext context, IConfiguration configuration)
        {
            context.Services.AddCors(options =>
            {
                options.AddPolicy(DefaultCorsPolicyName, builder =>
                {
                    builder
                        .WithOrigins(
                            configuration["App:CorsOrigins"]
                                .Split(",", StringSplitOptions.RemoveEmptyEntries)
                                .Select(o => o.RemovePostFix("/"))
                                .ToArray()
                        )
                        .WithAbpExposedHeaders()
                        .SetIsOriginAllowedToAllowWildcardSubdomains()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });
            });
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();
            var env = context.GetEnvironment();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            // 跨域
            app.UseCors(p => p.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            //   app.UseAbpRequestLocalization();

            if (!env.IsDevelopment())
            {
                app.UseErrorPage();
            }

            app.UseCorrelationId();
            app.UseVirtualFiles();
            app.UseRouting();
            app.UseCors(DefaultCorsPolicyName);
            app.UseAuthentication();

            if (MultiTenancyConsts.IsEnabled)
            {
                app.UseMultiTenancy();
            }

            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "SF API");

            });

            app.UseAuditing();
            app.UseAbpSerilogEnrichers();
            app.UseConfiguredEndpoints();
        }
    }
}
