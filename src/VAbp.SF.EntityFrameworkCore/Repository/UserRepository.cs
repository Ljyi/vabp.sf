﻿using System;
using System.Collections.Generic;
using System.Text;
using VAbp.SF.EntityFrameworkCore;
using VAbp.SF.System;
using VAbp.SF.System.Repository;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace VAbp.SF.Repository
{
    public class UserRepository : EfCoreRepository<SFDbContext, User, int>, IUserRepository
    {
        public UserRepository(IDbContextProvider<SFDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}