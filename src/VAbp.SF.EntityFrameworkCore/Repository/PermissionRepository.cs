﻿using System;
using System.Collections.Generic;
using System.Text;
using VAbp.SF.EntityFrameworkCore;
using VAbp.SF.System;
using VAbp.SF.System.Repository;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace VAbp.SF.Repository
{
    public class PermissionRepository : EfCoreRepository<SFDbContext, Permission, int>, IPermissionRepository
    {
        public PermissionRepository(IDbContextProvider<SFDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
