﻿using System;
using System.Collections.Generic;
using System.Text;
using VAbp.SF.EntityFrameworkCore;
using VAbp.SF.System;
using VAbp.SF.System.Repository;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace VAbp.SF.Repository
{
    public class ChannelRepository : EfCoreRepository<SFDbContext, Channel, int>, IChannelRepository
    {
        public ChannelRepository(IDbContextProvider<SFDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}

