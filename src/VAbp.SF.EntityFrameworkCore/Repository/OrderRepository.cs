﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.EntityFrameworkCore;
using VAbp.SF.System;
using VAbp.SF.System.Repository;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Text;

namespace VAbp.SF.Repository
{
    public class OrderRepository : EfCoreRepository<SFDbContext, Order, int>, IOrderRepository
    {
        public IDbContextProvider<SFDbContext> _dbContextProvider;
        public OrderRepository(IDbContextProvider<SFDbContext> dbContextProvider) : base(dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }
        /// <summary>
        /// 执行给定的命令
        /// </summary>
        /// <param name="sql">命令字符串</param>
        /// <param name="parameters">要应用于命令字符串的参数</param>
        /// <returns>执行命令后由数据库返回的结果</returns>
        public IQueryable<Order> GetOrders(string sql)
        {
            return _dbContextProvider.GetDbContext().Set<Order>().FromSqlRaw(sql);
        }
    }
}
