﻿using Microsoft.EntityFrameworkCore;
using VAbp.SF.System;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.Modeling;
using Volo.Abp.Identity;
using Volo.Abp.Users.EntityFrameworkCore;

namespace VAbp.SF.EntityFrameworkCore
{
    /* This is your actual DbContext used on runtime.
     * It includes only your entities.
     * It does not include entities of the used modules, because each module has already
     * its own DbContext class. If you want to share some database tables with the used modules,
     * just create a structure like done for AppUser.
     *
     * Don't use this DbContext for database migrations since it does not contain tables of the
     * used modules (as explained above). See SFMigrationsDbContext for migrations.
     */
    [ConnectionStringName("Default")]
    public class SFDbContext : AbpDbContext<SFDbContext>
    {
        //  public DbSet<AppUser> Users { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<MenuIcon> MenuIcon { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<Permission> Permission { get; set; }
        public DbSet<UserPermission> UserPermission { get; set; }
        public DbSet<Channel> Channel { get; set; }
        public DbSet<UserChanel> UserChanel { get; set; }


        /* Add DbSet properties for your Aggregate Roots / Entities here.
         * Also map them inside SFDbContextModelCreatingExtensions.ConfigureSF
         */

        public SFDbContext(DbContextOptions<SFDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            /* Configure the shared tables (with included modules) here */

            //builder.Entity<AppUser>(b =>
            //{
            //    b.ToTable(AbpIdentityDbProperties.DbTablePrefix + "Users"); //Sharing the same table "AbpUsers" with the IdentityUser

            //    b.ConfigureByConvention();
            //    b.ConfigureAbpUser();

            //    /* Configure mappings for your additional properties
            //     * Also see the SFEfCoreEntityExtensionMappings class
            //     */
            //});

            /* Configure your own tables/entities inside the ConfigureSF method */

            builder.ConfigureSF();
        }
    }
}
