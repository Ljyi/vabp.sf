﻿using Microsoft.EntityFrameworkCore;
using VAbp.SF.System;
using Volo.Abp;

namespace VAbp.SF.EntityFrameworkCore
{
    public static class SFDbContextModelCreatingExtensions
    {
        public static void ConfigureSF(this ModelBuilder builder)
        {
            /* Configure your own tables/entities inside here */

            //builder.Entity<YourEntity>(b =>
            //{
            //    b.ToTable(SFConsts.DbTablePrefix + "YourEntities", SFConsts.DbSchema);
            //    b.ConfigureByConvention(); //auto configure for the base class props
            //    //...
            //});
            Check.NotNull(builder, nameof(builder));
            builder.Entity<User>(b =>
            {
                b.ToTable("User");
                b.HasKey(x => x.Id);
                b.Property(x => x.Id).ValueGeneratedOnAdd();
                b.Property(x => x.UserName).HasMaxLength(20).IsRequired();
                b.Property(x => x.Account).HasMaxLength(20);
                b.Property(x => x.Password).HasMaxLength(100);
                b.Property(x => x.Email).HasMaxLength(100);
                b.Property(x => x.HeadImg).HasMaxLength(20);
                b.Property(x => x.Status).HasMaxLength(20).IsRequired();
                b.Property(x => x.Remark).HasMaxLength(200);
                b.Property(x => x.IsAdmin).HasColumnType("bit");
                b.Property(x => x.IsDelete).HasColumnType("bit");
                b.Property(x => x.IsLocked).HasColumnType("bit");
                b.Property(x => x.Forbidden).HasColumnType("bit");
                b.Property(x => x.CreateUser).HasMaxLength(20);
                b.Property(x => x.CredateTime).HasColumnType("datetime");
                b.Property(x => x.UpdateUser).HasMaxLength(20);
                b.Property(x => x.UpdateTime).HasColumnType("datetime");
            });
            builder.Entity<Order>(b =>
            {
                b.ToTable("Order");
                b.HasKey(x => x.Id);
                b.Property(x => x.Id).ValueGeneratedOnAdd();
                b.Property(x => x.OrderNo).HasMaxLength(50).IsRequired();
                b.Property(x => x.Channel).HasColumnType("int").IsRequired();
                b.Property(x => x.UserName).HasMaxLength(200);
                b.Property(x => x.Address).HasMaxLength(200);
                b.Property(x => x.Remark).HasMaxLength(50);
                b.Property(x => x.Phone).HasMaxLength(11);
                b.Property(x => x.Status).HasColumnType("int");
                b.Property(x => x.IsDelete).HasColumnType("bit");
                b.Property(x => x.IsSync).HasColumnType("bit");
                b.Property(x => x.CreateUser).HasMaxLength(20);
                b.Property(x => x.CredateTime).HasColumnType("datetime");
                b.Property(x => x.UpdateUser).HasMaxLength(20);
                b.Property(x => x.UpdateTime).HasColumnType("datetime");
            });
            builder.Entity<MenuIcon>(b =>
            {
                b.ToTable("MenuIcon");
                b.HasKey(x => x.Id);
                b.Property(x => x.Id).ValueGeneratedOnAdd();
                b.Property(x => x.Code).HasMaxLength(50).IsRequired();
                b.Property(x => x.Size).HasMaxLength(200);
                b.Property(x => x.Color).HasMaxLength(200);
                b.Property(x => x.Status).HasColumnType("int");
                b.Property(x => x.IsDelete).HasColumnType("bit");
                b.Property(x => x.CreateUser).HasMaxLength(20);
                b.Property(x => x.CreateTime).HasColumnType("datetime");
                b.Property(x => x.UpdateUser).HasMaxLength(20);
                b.Property(x => x.UpdateTime).HasColumnType("datetime");
            });
            builder.Entity<Menu>(b =>
            {
                b.ToTable("Menu");
                b.HasKey(x => x.Id);
                b.Property(x => x.Id).ValueGeneratedOnAdd();
                b.Property(x => x.MenuCode).HasMaxLength(100).IsRequired();
                b.Property(x => x.MenuName).HasMaxLength(100).IsRequired();
                b.Property(x => x.Url).HasMaxLength(100).IsRequired();
                b.Property(x => x.Alias).HasMaxLength(100);
                b.Property(x => x.MenuLevel).HasColumnType("int").IsRequired();
                b.Property(x => x.ParentId).HasColumnType("int").IsRequired();
                b.Property(x => x.Sort).HasMaxLength(100);
                b.Property(x => x.Icon).HasMaxLength(100);
                b.Property(x => x.Description).HasMaxLength(200);
                b.Property(x => x.Component).HasMaxLength(200);
                b.Property(x => x.HideInMenu).HasColumnType("bit");
                b.Property(x => x.IsShow).HasColumnType("bit");
                b.Property(x => x.NotCache).HasColumnType("bit");
                b.Property(x => x.Status).HasColumnType("int");
                b.Property(x => x.IsDelete).HasColumnType("bit");
                b.Property(x => x.CreateUser).HasMaxLength(20);
                b.Property(x => x.CreateTime).HasColumnType("datetime");
                b.Property(x => x.UpdateUser).HasMaxLength(20);
                b.Property(x => x.UpdateTime).HasColumnType("datetime");
            });
            builder.Entity<Permission>(b =>
            {
                b.ToTable("Permission");
                b.HasKey(x => x.Id);
                b.Property(x => x.Id).ValueGeneratedOnAdd();
                b.Property(x => x.Code).HasMaxLength(10).IsRequired();
                b.Property(x => x.MenuId).HasColumnType("int").IsRequired();
                b.Property(x => x.MenuAlias).HasMaxLength(100);
                b.Property(x => x.Name).HasMaxLength(20).IsRequired();
                b.Property(x => x.ActionCode).HasMaxLength(50).IsRequired();
                b.Property(x => x.Icon).HasMaxLength(50);
                b.Property(x => x.ActionUrl).HasMaxLength(200);
                b.Property(x => x.Description).HasMaxLength(200);
                b.Property(x => x.Status).HasColumnType("int");
                b.Property(x => x.Type).HasColumnType("int");
                b.Property(x => x.IsDelete).HasColumnType("bit");
                b.Property(x => x.CreateUser).HasMaxLength(20);
                b.Property(x => x.CreateTime).HasColumnType("datetime");
                b.Property(x => x.UpdateUser).HasMaxLength(20);
                b.Property(x => x.UpdateTime).HasColumnType("datetime");
            });
            builder.Entity<UserPermission>(b =>
            {
                b.ToTable("UserPermission");
                b.HasKey(x => x.Id);
                b.Property(x => x.Id).ValueGeneratedOnAdd();
                b.Property(x => x.UserId).HasColumnType("int").IsRequired();
                b.Property(x => x.PermissionId).HasColumnType("int").IsRequired();
                b.Property(x => x.IsDelete).HasColumnType("bit");
                b.Property(x => x.CreateUser).HasMaxLength(20);
                b.Property(x => x.CreateTime).HasColumnType("datetime");
                b.Property(x => x.UpdateUser).HasMaxLength(20);
                b.Property(x => x.UpdateTime).HasColumnType("datetime");
            });
            builder.Entity<Channel>(b =>
            {
                b.ToTable("Channel");
                b.HasKey(x => x.Id);
                b.Property(x => x.Id).ValueGeneratedOnAdd();
                b.Property(x => x.ChannelCode).HasMaxLength(100).IsRequired();
                b.Property(x => x.ChannelName).HasMaxLength(100).IsRequired();
                b.Property(x => x.ParentId).HasColumnType("int");
                b.Property(x => x.Sort).HasColumnType("int");
                b.Property(x => x.Enable).HasColumnType("bit");
                b.Property(x => x.IsDelete).HasColumnType("bit");
                b.Property(x => x.CreateUser).HasMaxLength(20);
                b.Property(x => x.CreateTime).HasColumnType("datetime");
                b.Property(x => x.UpdateUser).HasMaxLength(20);
                b.Property(x => x.UpdateTime).HasColumnType("datetime");
            });
            builder.Entity<UserChanel>(b =>
            {
                b.ToTable("UserChanel");
                b.HasKey(x => x.Id);
                b.Property(x => x.Id).ValueGeneratedOnAdd();
                b.Property(x => x.UserId).HasColumnType("int");
                b.Property(x => x.ChannelId).HasColumnType("int");
                b.Property(x => x.IsDelete).HasColumnType("bit");
                b.Property(x => x.CreateUser).HasMaxLength(20);
                b.Property(x => x.CreateTime).HasColumnType("datetime");
                b.Property(x => x.UpdateUser).HasMaxLength(20);
                b.Property(x => x.UpdateTime).HasColumnType("datetime");
            });
        }
    }
}