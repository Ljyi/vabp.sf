﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace VAbp.SF.Base
{
    public static class HttpRequestHelp
    {
        public static string Post(string requstUrl, string data, string username, string password)
        {
            string msg = "";
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}")));

                HttpContent httpContent = new StringContent(data, Encoding.UTF8);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                Uri address = new Uri(requstUrl);
                msg = client.PostAsync(address, httpContent).Result.Content.ReadAsStringAsync().Result;
            }
            return msg;
        }
    }
}
