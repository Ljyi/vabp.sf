﻿namespace VAbp.SF.Base
{
    public interface IPagedList<T> : IListResult<T>, IHasTotalCount
    {
    }
}