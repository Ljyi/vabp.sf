﻿

namespace VAbp.SF.Base
{
    public class AccessTokenRequest
    {
        /// <summary>
        /// Client ID
        /// </summary>
        public string Client_ID { get; set; }

        /// <summary>
        /// Client Secret
        /// </summary>
        public string Client_Secret { get; set; }

        /// <summary>
        /// 调用API_Authorize获取到的Code值
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Authorization callback URL
        /// </summary>
        public string Redirect_Uri { get; set; }

        /// <summary>
        /// State
        /// </summary>
        public string State { get; set; }
    }
}