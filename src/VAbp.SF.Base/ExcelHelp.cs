﻿using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace VAbp.SF.Base
{
    /// <summary>
    /// excel
    /// </summary>
    public class ExcelHelp
    {
        /// <summary>
        /// 导出excel
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static byte[] ExportToExcel<T>(List<T> entities, ExportModel model)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFPalette palette = workbook.GetCustomPalette();
            HSSFColor hssFColor;
            byte red, green, bule;

            if (model.DataFields == null || model.DataFields.Length == 0)
            {
                model.DataFields = model.ColumnNames;
            }
            #region 标题

            // 标题字体
            IFont titleFont = workbook.CreateFont();
            var titleColor = model.TitleRow.CellStyle.Font.Color;
            red = titleColor[0];
            green = titleColor[1];
            bule = titleColor[2];
            palette.SetColorAtIndex(8, red, green, bule);
            hssFColor = palette.FindColor(red, green, bule);
            titleFont.Color = hssFColor.Indexed;
            titleFont.FontHeightInPoints = model.TitleRow.CellStyle.Font.FontHeightInPoints;

            // 标题前景色
            var titleForegroundColor = model.TitleRow.CellStyle.FillForegroundColor;
            red = titleForegroundColor[0];
            green = titleForegroundColor[1];
            bule = titleForegroundColor[2];
            palette.SetColorAtIndex(9, red, green, bule);
            hssFColor = palette.FindColor(red, green, bule);

            // 标题
            ICellStyle titleStyle = workbook.CreateCellStyle();
            titleStyle.SetFont(titleFont);
            titleStyle.FillPattern = FillPattern.SolidForeground;
            titleStyle.FillForegroundColor = hssFColor.Indexed;
            titleStyle.Alignment = HorizontalAlignment.Center;
            titleStyle.VerticalAlignment = VerticalAlignment.Center;

            ISheet sheet = workbook.CreateSheet("Sheet1");
            IRow row = sheet.CreateRow(0);
            row.HeightInPoints = model.DataRow.HeightInPoints;
            ICell cell = null;
            for (int i = 0; i < model.ColumnNames.Length; i++)
            {
                cell = row.CreateCell(i);
                cell.CellStyle = titleStyle;
                cell.SetCellValue(model.ColumnNames[i]);
            }
            #endregion

            if (entities.Count > 0)
            {
                // 数据行
                object cellValue = string.Empty;
                ICellStyle cellStyle = workbook.CreateCellStyle();
                IFont cellFont = workbook.CreateFont();
                cellFont.FontHeightInPoints = model.DataRow.CellStyle.Font.FontHeightInPoints;
                cellStyle.SetFont(cellFont);
                cellStyle.VerticalAlignment = VerticalAlignment.Center;
                for (int i = 0; i < entities.Count; i++)
                {
                    row = sheet.CreateRow(i + 1);
                    row.HeightInPoints = model.DataRow.HeightInPoints;
                    object entity = entities[i];
                    for (int j = 0; j < model.DataFields.Length; j++)
                    {
                        cellValue = entity.GetType().GetProperty(model.DataFields[j]).GetValue(entity);
                        cell = row.CreateCell(j);
                        cell.CellStyle = cellStyle;
                        if (cellValue != null && cellValue.GetType() == typeof(bool))
                        {
                            if (cellValue.ToString() == "False")
                            {
                                cell.SetCellValue("否");
                            }
                            else
                            {
                                cell.SetCellValue("是");
                            }
                        }
                        else
                        {
                            cell.SetCellValue(Convert.ToString(cellValue));
                        }
                    }
                }
                // 调整列宽
                for (int i = 0; i <= entities.Count; i++)
                {
                    sheet.AutoSizeColumn(i);
                }
                for (int columnNum = 0; columnNum <= model.ColumnNames.Length; columnNum++)
                {
                    int columnWidth = sheet.GetColumnWidth(columnNum) / 256;
                    for (int rowNum = 1; rowNum <= sheet.LastRowNum; rowNum++)
                    {
                        IRow currentRow;
                        if (sheet.GetRow(rowNum) == null)
                        {
                            currentRow = sheet.CreateRow(rowNum);
                        }
                        else
                        {
                            currentRow = sheet.GetRow(rowNum);
                        }

                        if (currentRow.GetCell(columnNum) != null)
                        {
                            ICell currentCell = currentRow.GetCell(columnNum);
                            int length = Encoding.Default.GetBytes(currentCell.ToString()).Length;
                            if (columnWidth < length)
                            {
                                columnWidth = length;
                            }
                        }
                    }
                    columnWidth = Math.Min(columnWidth, 255);
                    sheet.SetColumnWidth(columnNum, columnWidth * 256);
                }
            }
            using (MemoryStream ms = new MemoryStream())
            {
                workbook.Write(ms);
                return ms.GetBuffer();
            }
        }


        public static string ExportListToExcel<T>(IList<T> list, ref string fileName, IList<string> headerList = null, IList<string> sortList = null, bool isCompress = false, int pagingSize = 65535, string fileNameNew = null)
        {
            try
            {
                //服务器路径
                var serverPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Export/Temp");
                if (!Directory.Exists(serverPath))
                {
                    Directory.CreateDirectory(serverPath);
                }
                var listFileNames = new List<string>();
                int pageIndex = 0;
                #region 生成文件名
                var random = new Random().Next(10000, 50000);
                var date = DateTime.Now.ToString("MMddhhmmss");
                #endregion
                //选取65536是因为Excel2003的Sheet的最大行，如果是列数有很多很多而导致内存溢出的，可以降低这个值
                pageIndex++;
                #region 生成文件名
                if (fileNameNew == null)
                {
                    fileName = fileNameNew + "_" + date + "_" + random + "_" + pageIndex;
                }
                if (!fileName.EndsWith(".xls") && !fileName.EndsWith(".xlsx"))
                {
                    fileName += ".xls";
                }
                var curExcelName = Path.Combine(serverPath, fileName);
                #endregion
                #region 创建临时文件
                using (var ms = new MemoryStream())
                {
                    var workbook = new HSSFWorkbook();   //创建Excel工作部   
                    workbook.Write(ms);//将Excel写入流
                    ms.Flush();
                    ms.Position = 0;
                    FileStream dumpFile = new FileStream(curExcelName, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                    ms.WriteTo(dumpFile);//将流写入文件
                    ms.Close();
                    dumpFile.Close();
                }
                #endregion
                #region 打开文件，写内容
                using (FileStream fs = File.Open(curExcelName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    IWorkbook workbook = new HSSFWorkbook(fs);
                    ICellStyle cellStyle = workbook.CreateCellStyle();
                    cellStyle.BorderTop = BorderStyle.Thin;
                    cellStyle.BorderBottom = BorderStyle.Thin;
                    cellStyle.BorderLeft = BorderStyle.Thin;
                    cellStyle.BorderRight = BorderStyle.Thin;
                    cellStyle.TopBorderColor = HSSFColor.Grey50Percent.Index;
                    cellStyle.BottomBorderColor = HSSFColor.Grey50Percent.Index;
                    cellStyle.RightBorderColor = HSSFColor.Grey50Percent.Index;
                    cellStyle.LeftBorderColor = HSSFColor.Grey50Percent.Index;
                    ISheet sheet = workbook.CreateSheet(string.Format("sheet{0}", pageIndex));
                    IRow row = sheet.CreateRow(0);
                    int count = 0;
                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                    //如果没有自定义的行首,那么采用反射集合的属性名做行首
                    if (headerList == null)
                    {
                        for (int i = 0; i < properties.Count; i++) //生成sheet第一行列名 
                        {
                            ICell cell = row.CreateCell(count++);
                            cell.CellStyle = cellStyle;
                            cell.SetCellValue(String.IsNullOrEmpty(properties[i].DisplayName) ? properties[i].Name : properties[i].DisplayName);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < headerList.Count; i++) //生成sheet第一行列名 
                        {
                            ICell cell = row.CreateCell(count++);
                            cell.CellStyle = cellStyle;
                            cell.SetCellValue(headerList[i]);
                        }
                    }
                    //将数据导入到excel表中
                    if (list == null) return "";
                    var sheetList = list.ToList();
                    for (int i = 0; i < sheetList.Count; i++)
                    {
                        IRow rows = sheet.CreateRow(i + 1);
                        count = 0;
                        object value = null;
                        //如果自定义导出属性及排序字段为空,那么走反射序号的方式
                        if (sortList == null)
                        {
                            for (int j = 0; j < properties.Count; j++)
                            {
                                ICell cell = rows.CreateCell(count++);
                                cell.CellStyle = cellStyle;
                                value = properties[j].GetValue(sheetList[i]);
                                if (value == null)
                                    cell.SetCellValue("");
                                else
                                {
                                    setCellValue(cell, properties[j].PropertyType.Name.ToLower(), value, () =>
                                    {
                                        return Nullable.GetUnderlyingType(properties[j].PropertyType).Name.ToLower();
                                    });
                                }
                            }
                        }
                        else
                        {
                            for (int j = 0; j < sortList.Count; j++)
                            {
                                ICell cell = rows.CreateCell(count++);
                                cell.CellStyle = cellStyle;
                                var currentProperty = properties[sortList[j]];
                                value = currentProperty.GetValue(sheetList[i]);
                                if (value == null)
                                    cell.SetCellValue("");

                                //根据反射来获取类型
                                setCellValue(cell, currentProperty.PropertyType.Name.ToLower(), value, () =>
                                {
                                    return Nullable.GetUnderlyingType(currentProperty.PropertyType).Name.ToLower();
                                });
                            }
                        }
                        sheet.ForceFormulaRecalculation = true;         //保存excel文档
                    }
                    using (FileStream fs2 = File.Create(curExcelName))
                    {
                        workbook.Write(fs2);
                        fs2.Close();
                    }

                    listFileNames.Add(curExcelName);
                }
                #endregion
                #region 处理文件名
                if (isCompress || listFileNames.Count > 1)
                {
                    var fzName = listFileNames.First().Replace(".xls", ".zip").Replace(".xlsx", ".zip");
                    return fzName;
                }
                return listFileNames.First();


                #endregion
            }
            catch (Exception ex)
            {
                throw ex.InnerException != null ? ex.InnerException : ex;
            }
        }

        public static byte[] ExportListToExcelByte<T>(IList<T> list, ref string fileName, IList<string> headerList = null, IList<string> sortList = null, bool isCompress = false, int pagingSize = 65535, string fileNameNew = null)
        {
            try
            {
                //服务器路径
                var serverPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Export/Temp");
                if (!Directory.Exists(serverPath))
                {
                    Directory.CreateDirectory(serverPath);
                }
                var listFileNames = new List<string>();
                int pageIndex = 0;
                #region 生成文件名
                var random = new Random().Next(10000, 50000);
                var date = DateTime.Now.ToString("MMddhhmmss");
                #endregion
                //选取65536是因为Excel2003的Sheet的最大行，如果是列数有很多很多而导致内存溢出的，可以降低这个值
                pageIndex++;
                #region 生成文件名
                if (fileNameNew == null)
                {
                    fileName = fileNameNew + "_" + date + "_" + random + "_" + pageIndex;
                }
                if (!fileName.EndsWith(".xls") && !fileName.EndsWith(".xlsx"))
                {
                    fileName += ".xls";
                }
                var curExcelName = Path.Combine(serverPath, fileName);
                #endregion
                #region 创建临时文件
                using (var ms = new MemoryStream())
                {
                    var workbook = new HSSFWorkbook();   //创建Excel工作部   
                    workbook.Write(ms);//将Excel写入流
                    ms.Flush();
                    ms.Position = 0;
                    FileStream dumpFile = new FileStream(curExcelName, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                    ms.WriteTo(dumpFile);//将流写入文件
                    ms.Close();
                    dumpFile.Close();
                }
                #endregion
                #region 打开文件，写内容
                using (FileStream fs = File.Open(curExcelName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    IWorkbook workbook = new HSSFWorkbook(fs);
                    ICellStyle cellStyle = workbook.CreateCellStyle();
                    cellStyle.BorderTop = BorderStyle.Thin;
                    cellStyle.BorderBottom = BorderStyle.Thin;
                    cellStyle.BorderLeft = BorderStyle.Thin;
                    cellStyle.BorderRight = BorderStyle.Thin;
                    cellStyle.TopBorderColor = HSSFColor.Grey50Percent.Index;
                    cellStyle.BottomBorderColor = HSSFColor.Grey50Percent.Index;
                    cellStyle.RightBorderColor = HSSFColor.Grey50Percent.Index;
                    cellStyle.LeftBorderColor = HSSFColor.Grey50Percent.Index;
                    ISheet sheet = workbook.CreateSheet(string.Format("sheet{0}", pageIndex));
                    IRow row = sheet.CreateRow(0);
                    int count = 0;
                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                    //如果没有自定义的行首,那么采用反射集合的属性名做行首
                    if (headerList == null)
                    {
                        for (int i = 0; i < properties.Count; i++) //生成sheet第一行列名 
                        {
                            ICell cell = row.CreateCell(count++);
                            cell.CellStyle = cellStyle;
                            cell.SetCellValue(String.IsNullOrEmpty(properties[i].DisplayName) ? properties[i].Name : properties[i].DisplayName);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < headerList.Count; i++) //生成sheet第一行列名 
                        {
                            ICell cell = row.CreateCell(count++);
                            cell.CellStyle = cellStyle;
                            cell.SetCellValue(headerList[i]);
                        }
                    }
                    //将数据导入到excel表中
                    if (list == null) return null;
                    var sheetList = list.ToList();
                    for (int i = 0; i < sheetList.Count; i++)
                    {
                        IRow rows = sheet.CreateRow(i + 1);
                        count = 0;
                        object value = null;
                        //如果自定义导出属性及排序字段为空,那么走反射序号的方式
                        if (sortList == null)
                        {
                            for (int j = 0; j < properties.Count; j++)
                            {
                                ICell cell = rows.CreateCell(count++);
                                cell.CellStyle = cellStyle;
                                value = properties[j].GetValue(sheetList[i]);
                                if (value == null)
                                    cell.SetCellValue("");
                                else
                                {
                                    setCellValue(cell, properties[j].PropertyType.Name.ToLower(), value, () =>
                                    {
                                        return Nullable.GetUnderlyingType(properties[j].PropertyType).Name.ToLower();
                                    });
                                }
                            }
                        }
                        else
                        {
                            for (int j = 0; j < sortList.Count; j++)
                            {
                                ICell cell = rows.CreateCell(count++);
                                cell.CellStyle = cellStyle;
                                var currentProperty = properties[sortList[j]];
                                value = currentProperty.GetValue(sheetList[i]);
                                if (value == null)
                                    cell.SetCellValue("");

                                //根据反射来获取类型
                                setCellValue(cell, currentProperty.PropertyType.Name.ToLower(), value, () =>
                                {
                                    return Nullable.GetUnderlyingType(currentProperty.PropertyType).Name.ToLower();
                                });
                            }
                        }
                        sheet.ForceFormulaRecalculation = true;         //保存excel文档
                    }
                    using (FileStream fs2 = File.Create(curExcelName))
                    {
                        workbook.Write(fs2);
                        fs2.Close();
                    }
                    using (MemoryStream ms = new MemoryStream())
                    {
                        workbook.Write(ms);
                        return ms.GetBuffer();
                    }
                    //  listFileNames.Add(curExcelName);
                }
                #endregion
                #region 处理文件名
                //if (isCompress || listFileNames.Count > 1)
                //{
                //    var fzName = listFileNames.First().Replace(".xls", ".zip").Replace(".xlsx", ".zip");
                //    return fzName;
                //}
                //  return GetExcelByte(listFileNames.First());
                #endregion
            }
            catch (Exception ex)
            {
                throw ex.InnerException != null ? ex.InnerException : ex;
            }
        }


        private static void setCellValue(ICell cell, string typeName, object value, Func<string> getNullnderlyingType)
        {
            try
            {
                if (typeName == "nullable`1")
                {
                    if (string.IsNullOrWhiteSpace(value.ToString()))
                    {
                        cell.SetCellValue(string.Empty);
                    }
                    else
                    {
                        setCellValue(cell, getNullnderlyingType(), value, null);
                    }
                }
                else
                {
                    switch (typeName)
                    {
                        case "int":
                        case "int32":
                            cell.SetCellValue(Convert.ToInt32(value));
                            break;
                        case "long":
                        case "int64":
                            cell.SetCellValue(Convert.ToInt64(value));
                            break;
                        case "decimal":
                        case "float":
                        case "single":
                            cell.SetCellValue(Convert.ToDouble(value));
                            break;
                        case "datetime":
                            cell.SetCellValue(Convert.ToDateTime(value).ToString("yyyy-MM-dd HH:mm:ss"));
                            break;
                        case "bool":
                        case "boolean":
                            cell.SetCellValue(Convert.ToBoolean(value));
                            break;
                        default:
                            cell.SetCellValue(Convert.ToString(value));
                            break;
                    }
                }
            }
            catch (Exception)
            {
                cell.SetCellValue(string.Empty);
            }
        }

        public static MemoryStream GetExcelStream(string filePath)
        {
            MemoryStream ms = new MemoryStream();
            FileStream file = new FileStream(filePath, FileMode.Create, FileAccess.Read);
            byte[] bytes = new byte[file.Length];
            file.Read(bytes, 0, (int)file.Length);
            ms.Write(bytes, 0, (int)file.Length);
            file.Close();
            ms.Close();
            return ms;
        }
        public static byte[] GetExcelByte(string filePath)
        {
            using (FileStream file = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                byte[] bytes = new byte[file.Length];
                return bytes;
            };
        }
    }
}
