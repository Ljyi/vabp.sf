﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VAbp.SF.Models;
using VAbp.SF.System;

namespace VAbp.SF
{
    /// <summary>
    /// 
    /// </summary>
    public static class MenuItemHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="menus"></param>
        /// <param name="selectedGuid"></param>
        /// <returns></returns>
        public static List<MenuItem> BuildTree(this List<MenuItem> menus, string selectedGuid)
        {
            var lookup = menus.ToLookup(x => x.ParentId);

            List<MenuItem> Build(string pid)
            {
                return lookup[pid]
                    .Select(x => new MenuItem()
                    {
                        Id = x.Id,
                        ParentId = x.ParentId,
                        Children = Build(x.Id),
                        Component = x.Component ?? "Main",
                        Name = x.Name,
                        Path = x.Path,
                        Meta = new MenuMeta
                        {
                            BeforeCloseFun = x.Meta.BeforeCloseFun,
                            HideInMenu = x.Meta.HideInMenu,
                            Icon = x.Meta.Icon,
                            NotCache = x.Meta.NotCache,
                            Title = x.Meta.Title,
                            Permission = x.Meta.Permission,
                            IsShow = x.Meta.IsShow
                        }
                    }).ToList();
            }

            var result = Build(selectedGuid);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="menus"></param>
        /// <param name="selectedGuid"></param>
        /// <returns></returns>
        public static List<MenuItem> LoadMenuTree(List<MenuInfo> menus, string selectedGuid)
        {
            var temp = menus.Select(x => new MenuItem
            {
                Id = x.Id.ToString(),
                ParentId = x.ParentId.ToString(),
                Name = x.Alias,
                Path = $"/{x.Url}",
                Component = x.Component,
                Meta = new MenuMeta
                {
                    BeforeCloseFun = "",
                    HideInMenu = x.HideInMenu,
                    Icon = x.Icon,
                    NotCache = x.NotCache,
                    IsShow = x.IsShow,
                    Title = x.MenuName
                }
            }).ToList();
            var tree = temp.BuildTree(selectedGuid);
            return tree;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menus"></param>
        /// <param name="selectedId"></param>
        /// <returns></returns>
        public static List<MenuTree> BuildMenuTree(this List<MenuTree> menus, int? selectedId)
        {
            var lookup = menus.ToLookup(x => x.ParentId);
            Func<int?, List<MenuTree>> build = null;
            build = pid =>
            {
                return lookup[pid]
                 .Select(x => new MenuTree()
                 {
                     Id = x.Id,
                     ParentId = x.ParentId,
                     Title = x.Title,
                     Expand = x.ParentId == 0,
                     Selected = selectedId.Value == x.Id,
                     Children = build(x.Id),
                 })
                 .ToList();
            };
            var result = build(null);
            return result;
        }
    }
}
