﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.Models;
using VAbp.SF.System;
using VAbp.SF.System.Query;

namespace VAbp.SF.Controllers
{
    /// <summary>
    /// 订单
    /// </summary>
    [Route("api/v1/order")]
    [ApiExplorerSettings(GroupName = "V1")]
    [ApiController]
    public class OrderController : BaseController
    {
        IChannelService _channelService;
        IOrderService _orderService;
        IUserChanelService userChanelService;
        public OrderController(IOrderService orderService, IUserChanelService userChanelService, IChannelService channelService)
        {
            _channelService = channelService;
            this._orderService = orderService;
            this.userChanelService = userChanelService;
        }
        /// <summary>
        /// 获取渠道信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("info")]
        [HttpGet]
        public async Task<ResultModel<OrderInfo>> GetChannelInfo(int id)
        {
            ResultModel<OrderInfo> resultModel = new ResultModel<OrderInfo>();
            try
            {
                resultModel.data = await _orderService.GetOrderById(id);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 获取渠道
        /// </summary>
        /// <returns></returns>
        [Route("query")]
        [HttpPost]
        public ResultPage<OrderDto> GetOrderGrid(OrderQuery orderQuery)
        {
            ResultPage<OrderDto> resultModel = new ResultPage<OrderDto>();
            try
            {
                List<int> chanelIds = new List<int>();
                if (IsAdmin)
                {
                    chanelIds = _channelService.GetChannelAll().Select(zw => zw.Id).ToList();
                }
                else
                {
                    chanelIds = userChanelService.GetUserChanelByUserId(UserId.Value);
                }
                if (orderQuery.Channel.Count > 0 && orderQuery.Channel[0] != 0)
                {
                    orderQuery.Channel = orderQuery.Channel.Where(zw => chanelIds.Contains(zw)).ToList();
                }
                else
                {
                    orderQuery.Channel = chanelIds;
                }
                orderQuery.UserId = UserId.Value;
                PageJson<OrderDto> pageJson = new PageJson<OrderDto>();
                PageParameter pg = orderQuery;
                List<OrderDto> orderInfos = _orderService.GetOrderGrid(orderQuery, 0, IsAdmin);
                var channels = _channelService.Entities();
                orderInfos.ForEach(zw =>
                {
                    zw.ChannelName = channels.FirstOrDefault(t => t.Id == zw.Channel).ChannelName;
                });
                pageJson.Items = orderInfos;

                pageJson.TotalCount = pg.TotalCount;
                resultModel.data = pageJson;
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <returns></returns>
        [Route("delete")]
        [HttpPost]
        public async Task<ResultModel<bool>> DeleteChannel(int id)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                if (IsAdmin)
                {
                    resultModel.data = await _orderService.DeleteAsync(id, UserName);
                }
                else
                {
                    resultModel.success = false;
                    resultModel.message = "非管理员不能删除";
                    resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
                }
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }

        /// <summary>
        /// 导出中奖数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("export")]
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ExportToExcel()
        {
            OrderQuery orderQuery = new OrderQuery();
            List<int> chanelIds = new List<int>();
            if (IsAdmin)
            {
                chanelIds = _channelService.GetChannelAll().Select(zw => zw.Id).ToList();
            }
            else
            {
                chanelIds = userChanelService.GetUserChanelByUserId(UserId.Value);
            }
            if (orderQuery.Channel.Count > 0)
            {
                orderQuery.Channel = orderQuery.Channel.Where(zw => chanelIds.Contains(zw)).ToList();
            }
            else
            {
                orderQuery.Channel = chanelIds;
            }
            orderQuery.UserId = UserId.Value;
            var result = _orderService.GetOrderGrid(orderQuery, 1, IsAdmin);
            if (result == null)
            {
                Content("数据不存在");
            }
            List<OrderExportModel> orderExportModelList = new List<OrderExportModel>();
            List<OrderExport> orderExportList = new List<OrderExport>();
            if (result.Count > 0)
            {
                //orderExportModelList = result.Select(zw => new OrderExportModel()
                //{
                //    Address = zw.Address,
                //    Date = zw.CredateTime.ToShortDateString(),
                //    OrderNo = zw.OrderNo,
                //    Phone = zw.Phone,
                //    Remark = zw.Remark,
                //    Time = zw.CredateTime.ToLongTimeString(),
                //    UserName = zw.UserName
                //}).ToList();
                orderExportList = result.Select(zw => new OrderExport()
                {
                    地址 = zw.Address,
                    日期 = zw.CredateTime.ToShortDateString(),
                    订单号 = zw.OrderNo,
                    手机号 = zw.Phone,
                    备注 = zw.Remark,
                    时间 = zw.CredateTime.ToLongTimeString(),
                    姓名 = zw.UserName
                }).ToList();
            }
            //ExportModel excelModel = new ExportModel();
            //excelModel.DataFields = new string[] { "OrderNo", "UserName", "Phone", "Address", "Remark", "Date", "Time" };
            //excelModel.ColumnNames = new string[] { "订单号", "姓名", "手机号", "地址", "备注", "日期", "时间" };
            //var buffer = ExcelHelp.ExportToExcel(orderExportModelList, excelModel);
            string fileName = UserId.Value.ToString() + "_订单" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls";
            var buffer = ExcelHelp.ExportListToExcelByte<OrderExport>(orderExportList, ref fileName);
            return File(buffer, MediaTypeNames.Application.Octet, fileName);
        }

        /// <summary>
        /// 获取订单数量
        /// </summary>
        /// <returns></returns>
        [Route("ordercount")]
        [HttpGet]
        public ResultModel<List<OrderCount>> GetOrderCount()
        {
            ResultModel<List<OrderCount>> resultModel = new ResultModel<List<OrderCount>>();
            try
            {
                List<int> chanelIds = new List<int>();
                if (IsAdmin)
                {
                    chanelIds = _channelService.GetChannelAll().Select(zw => zw.Id).ToList();
                }
                else
                {
                    chanelIds = userChanelService.GetUserChanelByUserId(UserId.Value);
                }
                resultModel.data = _orderService.GetOrderCounts(chanelIds);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 渠道订单
        /// </summary>
        /// <returns></returns>
        [Route("select")]
        [HttpGet]
        public ResultModel<List<OrderChannel>> GetOrderChannel()
        {
            ResultModel<List<OrderChannel>> resultModel = new ResultModel<List<OrderChannel>>();
            List<int> chanelIds = new List<int>();
            if (IsAdmin)
            {
                chanelIds = _channelService.GetChannelAll().Select(zw => zw.Id).ToList();
            }
            else
            {
                chanelIds = userChanelService.GetUserChanelByUserId(UserId.Value);
            }
            if (chanelIds.Count > 0)
            {
                resultModel.data = _orderService.GetOrderChannel(chanelIds, UserId.Value);
            }
            return resultModel;
        }
    }
}
