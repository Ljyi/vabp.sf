﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VAbp.SF.Auth.Jwt;
using VAbp.SF.Auth.Policys;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.Models;
using VAbp.SF.System;
using Volo.Abp.Authorization;

namespace VAbp.SF.Controllers
{
    /// <summary>
    /// 授权
    /// </summary>
    [Route("api/oauth")]
    [ApiExplorerSettings(GroupName = "V1")]
    [ApiController]
    public class OauthController : BaseController
    {
        IUserService userService;
        // IPooledRedisManagerServcie pooledRedisManagerServcie;
        PermissionRequirementM _requirement;
        public OauthController(IUserService userService, PermissionRequirementM requirement)//, IPooledRedisManagerServcie pooledRedisManagerServcie,PermissionRequirementM requirement,
        {
            // this.pooledRedisManagerServcie = pooledRedisManagerServcie;
            this.userService = userService;
            _requirement = requirement;
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResultModel<string>> LoginAsync(LoginModel loginModel, CancellationToken cancellationToken)
        {
            ResultModel<string> resultModel = new ResultModel<string>();
            //try
            //{
            UserInfo user = await userService.Login(loginModel.Account, loginModel.Password);
            if (user != null)
            {
                TokenInfo tokenInfo = await GetJwtToken(user);
                resultModel.data = tokenInfo.Access_Token;
            }
            else
            {
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.ServerError.ToString());
                resultModel.success = false;
                resultModel.message = "账号或密码有误";
            }
            return resultModel;
        }

        /// <summary>
        /// 注销
        /// </summary>
        /// <returns></returns>
        [Route("logout")]
        [HttpGet]
        public ResultModel<bool> Logout()
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                //string token = HttpContext.Request.Headers["Authorization"].Single().Split(" ").Last();
                //RemoveTokenCashe(UserId.Value.ToString());
                resultModel.data = true;// await jwtAppCacheService.RemoveToken(UserId.Value.ToString(), token);
            }
            catch (Exception ex)
            {
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.ServerError.ToString());
                resultModel.success = false;
                resultModel.message = ex.Message;
            }
            return resultModel;
        }
        /// <summary>
        /// 获取token
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<TokenInfo> GetJwtToken(UserInfo user)
        {
            string RoleName = "staff";
            if (user.IsAdmin)
            {
                RoleName = "admin";
            }
            _requirement.SId = user.Id.ToString();
            _requirement.Permissions = new List<Auth.Policys.Permission>();
            return await Task.Run(() =>
            {
                var claims = new List<Claim>
                      {
                        new Claim(ClaimTypes.Sid,user.Id.ToString()),
                        new Claim(ClaimTypes.Name, user.UserName),
                        new Claim(ClaimTypes.Role,RoleName),
                        new Claim("Password",user.Password),
                        new Claim(ClaimTypes.Expiration, DateTime.Now.AddSeconds(_requirement.Expiration.TotalSeconds).ToString())
                      };
                //用户标识
                var identity = new ClaimsIdentity(JwtBearerDefaults.AuthenticationScheme);
                identity.AddClaims(claims);
                var token = JwtToken.BuildJwtToken(claims.ToArray(), _requirement);
                SetTokenCashe(user.Id.ToString(), token.Access_Token);
                return token;
            });
        }


        /// <summary>
        /// 缓存token
        /// </summary>
        /// <param name="appUserId"></param>
        /// <param name="token"></param>
        private void SetTokenCashe(string appUserId, string token)
        {
            //  pooledRedisManagerServcie.Add(appUserId, token, RedisDB.Default05);
        }
        /// <summary>
        /// 移除token
        /// </summary>
        /// <param name="appUserId"></param>
        /// <param name="token"></param>
        private void RemoveTokenCashe(string appUserId)
        {
            //  pooledRedisManagerServcie.Remove(appUserId, RedisDB.Default05);
        }
    }
}
