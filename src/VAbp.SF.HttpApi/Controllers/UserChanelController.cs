﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.Models;
using VAbp.SF.System;
using Volo.Abp.Uow;

namespace VAbp.SF.Controllers
{
    /// <summary>
    /// 用户渠道
    /// </summary>
    [Route("api/v1/userchanel")]
    [ApiExplorerSettings(GroupName = "V1")]
    [ApiController]
    public class UserChanelController : BaseController
    {
        IUserChanelService _userChanelService;
        IChannelService _channelService;
        public UserChanelController(IUserChanelService userChanelService, IChannelService channelService)
        {
            this._userChanelService = userChanelService;
            this._channelService = channelService;
        }
        /// <summary>
        /// 获取渠道信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[Route("select")]
        //[HttpGet]
        //public ResultModel<List<ChannelTree>> GetUserChannelTree()
        //{
        //    ResultModel<List<ChannelTree>> resultModel = new ResultModel<List<ChannelTree>>();
        //    try
        //    {
        //        resultModel.data = _channelService.GetChannelTree();
        //    }
        //    catch (Exception ex)
        //    {
        //        resultModel.success = false;
        //        resultModel.message = ex.Message;
        //        resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
        //    }
        //    return resultModel;
        //}
        /// <summary>
        /// 获取用户权限
        /// </summary> 
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        [Route("chanelpermission")]
        [HttpGet]
        public ResultModel<List<int>> GetUserPermission(int userId)
        {
            ResultModel<List<int>> resultModel = new ResultModel<List<int>>();
            var userChanelPermissions = _userChanelService.Entities().Where(zw => zw.UserId == userId).ToList();
            if (userChanelPermissions != null & userChanelPermissions.Count > 0)
            {
                resultModel.data = userChanelPermissions.Select(zw => zw.ChannelId).ToList();
            }
            return resultModel;
        }
        /// <summary>
        /// 设置权限
        /// </summary>
        /// <param name="model">权限</param>
        /// <returns></returns>
        [Route("setchanelpermission")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResultModel<bool>> SetUserPermission(UserChanelModel userChanelModel)
        {
            string userName = "13";
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                if (userChanelModel.model.UserId < 1)
                {
                    resultModel.success = false;
                    resultModel.message = "用户Id无效";
                    resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
                }
                else
                {
                    var userChanelIds = userChanelModel.model.ChanelIds.Distinct().ToList();
                    var userChanelPermissions = _userChanelService.Entities().Where(zw => zw.UserId == userChanelModel.model.UserId && !userChanelIds.Contains(zw.ChannelId)).Select(zw => zw.Id).ToList();
                    //userChanelPermissions = userChanelPermissions.Where(zw => !userChanelIds.Contains(zw)).ToList();
                    //foreach (var item in userChanelPermissions)
                    //{
                    //    if (userChanelIds.Contains(zw=>zw))
                    //    {
                    //    }
                    //}
                    if (userChanelPermissions.Count > 0)
                    {
                        await _userChanelService.BlukDeleteAsync(userChanelPermissions);
                    }
                    userChanelPermissions = _userChanelService.Entities().Where(zw => zw.UserId == userChanelModel.model.UserId).Select(zw => zw.ChannelId).ToList();
                    List<UserChanel> userPermissions = userChanelModel.model.ChanelIds.Distinct().Where(zw => !userChanelPermissions.Contains(zw)).Select(zw =>
                    new UserChanel()
                    {
                        ChannelId = zw,
                        UserId = userChanelModel.model.UserId,
                        CreateTime = DateTime.Now,
                        CreateUser = userName,
                        IsDelete = false
                    }
                    ).ToList();
                    await _userChanelService.BlukInsertAsync(userPermissions);
                }
                resultModel.data = true;
                resultModel.code = "200";
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }

    }
}
