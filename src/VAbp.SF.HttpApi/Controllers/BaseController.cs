﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Security.Claims;
using VAbp.SF.Localization;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;

namespace VAbp.SF.Controllers
{
    /// <summary>
    /// 公共控制器
    /// </summary>
    [Authorize("Permission")]
    [ApiController]
    public abstract class BaseController : AbpController
    {
        protected BaseController()
        {
            LocalizationResource = typeof(SFResource);
        }
        /// <summary>
        /// 获取用户登录Id
        /// </summary>
        public int? UserId
        {
            get
            {
                try
                {
                    return int.Parse(base.HttpContext.User.Claims.FirstOrDefault(a => a.Type == ClaimTypes.Sid).Value);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        /// <summary>
        /// 获取用户名称
        /// </summary>
        public string UserName
        {
            get
            {
                try
                {
                    return base.HttpContext.User.Claims.FirstOrDefault(a => a.Type == ClaimTypes.Name).Value;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        /// <summary>
        /// 获取角色
        /// </summary>
        public string RoleName
        {
            get
            {
                try
                {
                    return base.HttpContext.User.Claims.FirstOrDefault(a => a.Type == ClaimTypes.Role).Value;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        /// <summary>
        /// 是否是管理员
        /// </summary>
        public bool IsAdmin
        {
            get
            {
                try
                {
                    if (RoleName.ToLower() == "admin")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// 获取token
        /// </summary>
        public string Token
        {
            get
            {
                try
                {
                    return base.HttpContext.User.Claims.FirstOrDefault(a => a.Type == "Token")?.Value;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }

    //public abstract class SFController : AbpController
    //{
    //    protected SFController()
    //    {
    //        LocalizationResource = typeof(SFResource);
    //    }
    //}
}