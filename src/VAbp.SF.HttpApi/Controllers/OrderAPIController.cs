﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.System;

namespace VAbp.SF.Controllers
{
    /// <summary>
    /// 订单
    /// </summary>
    [Route("api/order")]
    [ApiExplorerSettings(GroupName = "V1")]
    [ApiController]
    public class OrderAPIController : BaseController
    {
        static string shitWord = "你妈|死|操你|草你|尼玛|傻逼|爹|他妈|牛逼|妈逼|哈哈|骗|草泥|爸爸|干你|呵呵|破烂|撒比|母亲|妈的|你大爷|新疆|西藏|宁波市|宁波|德宏市|临沧市|普洱市|保山市|红河州|西双版纳";
        static List<string> shitWordList = shitWord.Split('|').ToList();
        private IHttpContextAccessor _accessor;
        IOrderService _orderService;
        public OrderAPIController(IOrderService orderService, IHttpContextAccessor accessor)
        {
            _accessor = accessor;
            this._orderService = orderService;
        }
        /// <summary>
        /// 获取渠道信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("order-add")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResultModel<bool>> OrderAdd(AddOrderDto orderDto)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                if (orderDto.Phone.Trim().Length != 11)
                {
                    resultModel.data = false;
                    resultModel.code = "1000";
                    resultModel.message = "手机号不正确";
                    return resultModel;
                }
                foreach (var item in shitWordList)
                {
                    orderDto.Address = orderDto.Address.Replace(item, "");
                    orderDto.UserName = orderDto.UserName.Replace(item, "");
                    orderDto.Remark = orderDto.Remark.Replace(item, "");
                }
                if (orderDto.Channel == 19)
                {
                    orderDto.Remark = "SSSSS";
                }
                resultModel.data = await _orderService.Add(orderDto);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 获取渠道信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("order-add-form")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> OrderAddFromAsync([FromForm] AddOrderDto orderDto)
        {
            try
            {
                bool success = false;
                foreach (var item in shitWordList)
                {
                    if (orderDto.Address.Contains(item))
                    {
                        success = true;
                        break;
                    }
                    if (orderDto.Remark.Contains(item))
                    {
                        success = true;
                        break;
                    }
                    //orderDto.Address = orderDto.Address.Replace(item, "");
                    //orderDto.UserName = orderDto.UserName.Replace(item, "");
                    //orderDto.Remark = orderDto.Remark.Replace(item, "");
                }
                if (orderDto.Phone.Trim().Length == 11 && !success)
                {
                    await _orderService.Add(orderDto);
                }
            }
            catch (Exception ex)
            {
                //resultModel.success = false;
                //resultModel.message = ex.Message;
                //resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            var httpcontext = _accessor.HttpContext;
            // return  httpcontext.Response.Redirect("http://order8.cynet.vip/order/wfdata/item/?wfid=SSS", true);
            if (orderDto.Channel == 7)
            {
                return Redirect("http://order8.cynet.vip/order/wfdata/item/od4.html");
            }
            if (orderDto.Channel==69)
            {
                return Redirect("http://order8.cynet.vip/s/t69.html");
            }
            if (orderDto.Channel == 47)
            {
                return Redirect("http://order8.cynet.vip/s/t27.html");
            }
            if (orderDto.Channel == 71)
            {
                return Redirect("http://order8.cynet.vip/s/t18.html");
            }
            if (orderDto.Channel == 72)
            {
                return Redirect("http://fun3.cynet.vip/t72.html");
            }
            if (orderDto.Channel == 73)
            {
                return Redirect("http://fun3.cynet.vip/t73.html");
            }
            if (orderDto.Channel == 52 || orderDto.Channel == 61)
            {
                return Redirect("http://order8.cynet.vip/s/m52.html");
            }
            if (orderDto.Channel == 54)
            {
                return Redirect("http://order8.cynet.vip/s/m53.html ");
            }
            if (orderDto.Channel == 36)
            {
                return Redirect("http://order8.cynet.vip/s/t36.html");
            }
            if (orderDto.Channel == 37)
            {
                return Redirect("http://order8.cynet.vip/s/t37.html");
            }
            if (orderDto.Channel == 38)
            {
                return Redirect("http://order8.cynet.vip/s/h29.html");
            }
            if (orderDto.Channel == 34)
            {
                return Redirect("http://order8.cynet.vip/s/t34.html");
            }
            if (orderDto.Channel == 35)
            {
                return Redirect("http://order8.cynet.vip/s/t35.html");
            }
            if (orderDto.Channel == 39)
            {
                return Redirect("http://order8.cynet.vip/s/h30.html");
            }
            if (orderDto.Channel == 31)
            {
                return Redirect("http://order8.cynet.vip/s/h31.html");
            }
            if (orderDto.Channel == 28)
            {
                return Redirect("http://order8.cynet.vip/s/h28.html");
            }
            else if (orderDto.Channel == 26)
            {
                return Redirect("http://order8.cynet.vip/s/t26.html");
            }
            else if (orderDto.Channel == 40 || orderDto.Channel == 51)
            {
                return Redirect("http://order8.cynet.vip/s/t40.html");
            }
            else if (orderDto.Channel == 41 || orderDto.Channel == 49)
            {
                return Redirect("http://order8.cynet.vip/s/t41.html");
            }
            else if (orderDto.Channel == 55)
            {
                return Redirect("http://order8.cynet.vip/s/t47.html");
            }
            else if (orderDto.Channel == 53)
            {
                return Redirect("http://order8.cynet.vip/s/t42.html");
            }
            else if (orderDto.Channel == 57 || orderDto.Channel == 42 || orderDto.Channel == 53 || orderDto.Channel == 58)
            {
                return Redirect("http://order8.cynet.vip/s/t42.html");
            }
            else if (orderDto.Channel == 56 || orderDto.Channel == 63 || orderDto.Channel == 64)
            {
                return Redirect("http://order8.cynet.vip/s/t45.html");
            }
            else if (orderDto.Channel == 46)
            {
                return Redirect("http://order8.cynet.vip/s/t46.html");
            }
            else if (orderDto.Channel == 47 || orderDto.Channel == 62)
            {
                return Redirect("http://order8.cynet.vip/s/t47.html");
            }
            else if (orderDto.Channel == 19)
            {
                return Redirect("http://order8.cynet.vip/s/t19.html");
            }
            else if (orderDto.Channel == 27)
            {
                return Redirect("http://order8.cynet.vip/s/h27.html");
            }
            else if (orderDto.Channel == 32)
            {
                return Redirect("http://order8.cynet.vip/s/t22.html");
            }
            else if (orderDto.Channel == 23 || orderDto.Channel == 66)
            {
                return Redirect("http://order8.cynet.vip/s/t23.html");
            }
            else if (orderDto.Channel == 33)
            {
                return Redirect("http://order8.cynet.vip/s/t24.html");
            }
            else if (orderDto.Channel == 26)
            {
                return Redirect("http://order8.cynet.vip/s/t26.html");
            }
            else if (orderDto.Channel == 18)
            {
                return Redirect("http://order8.cynet.vip/order/wfdata/item/lkl");
            }
            else if (orderDto.Channel == 18)
            {
                return Redirect("http://order8.cynet.vip/order/wfdata/item/lkl");
            }
            //else if (orderDto.Channel == 11)
            //{
            //    return Redirect("http://order8.cynet.vip/order/wfdata/item/od5.html");
            //}
            else if (orderDto.Channel == 12)
            {
                return Redirect("http://order8.cynet.vip/order/wfdata/item/od6.html");
            }
            //else if (orderDto.Channel == 13)
            //{
            //    return Redirect("http://order8.cynet.vip/order/wfdata/item/od7.html");
            //}
            else if (orderDto.Channel == 1)
            {
                return Redirect("http://order8.cynet.vip/s/s1.html");
            }
            else if (orderDto.Channel == 13)
            {
                return Redirect("http://order8.cynet.vip/s/s13.html");
            }
            else if (orderDto.Channel == 16 || orderDto.Channel == 60 || orderDto.Channel == 65 || orderDto.Channel == 68)//
            {
                return Redirect("http://order8.cynet.vip/s/s16.html");
            }
            else if (orderDto.Channel == 21)
            {
                return Redirect("http://order8.cynet.vip/s/s21.html");
            }
            else if (orderDto.Channel == 25 || orderDto.Channel == 59 || orderDto.Channel == 62)
            {
                return Redirect("http://order8.cynet.vip/s/s25.html");
            }
            else if (orderDto.Channel == 10)
            {
                return Redirect("http://order8.cynet.vip/s/t10.html");
            }
            else if (orderDto.Channel == 11)
            {
                return Redirect("http://order8.cynet.vip/s/t11.html");
            }
            else if (orderDto.Channel == 19)
            {
                return Redirect("http://order8.cynet.vip/s/t19.html");
            }
            else if (orderDto.Channel == 27)//|| orderDto.Channel == 68
            {
                return Redirect("http://order8.cynet.vip/s/h27.html");
            }
            else if (orderDto.Channel == 26)
            {
                return Redirect("http://order8.cynet.vip/s/t26.html");
            }
            else
            {
                return Redirect("http://order8.cynet.vip/order/wfdata/item/?wfid=SSS");
            }
        }
        /// <summary>
        /// 获取渠道信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("order-add-body")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResultModel<bool>> OrderAddBody([FromBody] AddOrderDto orderDto)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                foreach (var item in shitWordList)
                {
                    orderDto.Address = orderDto.Address.Replace(item, "");
                    orderDto.UserName = orderDto.UserName.Replace(item, "");
                    orderDto.Remark = orderDto.Remark.Replace(item, "");
                }
                resultModel.data = await _orderService.Add(orderDto);
                string tr = "http://order8.cynet.vip/order/wfdata/item/?wfid=SSS";
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }


    }
}
