﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using VAbp.SF.Auth.Policys;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.Models;
using VAbp.SF.System;
using VAbp.SF.System.Query;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Uow;

namespace VAbp.SF.Controllers
{
    /// <summary>
    /// 用户
    /// </summary>
    [ApiExplorerSettings(GroupName = "V1")]
    [Route("api/v1/user")]
    public class UserController : BaseController
    {
        IUserService userService;
        IUserPermissionService userPermissionService;
        //  PermissionRequirementM _requirement;
        IHostingEnvironment hostingEnvironment;
        public UserController(IUserService userService, IUserPermissionService userPermissionService, IHostingEnvironment hostingEnvironment)//PermissionRequirementM requirement,
        {
            //  _requirement = requirement;
            this.userService = userService;
            this.userPermissionService = userPermissionService;
            this.hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// 根据URL获取文章详情
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("add")]
        public async Task<ResultModel<bool>> AddUser(AddUserDto addUser)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                if (userService.Entities().Any(zw => !zw.IsDelete && (zw.UserName == addUser.UserName || zw.Account == addUser.Account)))
                {
                    resultModel.success = false;
                    resultModel.message = "账号或者用户已存在";
                    resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
                }
                else
                {
                    addUser.CreateUser = UserName;
                    addUser.CredateTime = DateTime.Now;
                    resultModel.data = await userService.AddUser(addUser);
                }
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 分页查询列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("query")]
        public async Task<ResultPage<UserInfo>> Query(UserQuery query)
        {
            ResultPage<UserInfo> resultModel = new ResultPage<UserInfo>();
            try
            {
                PageJson<UserInfo> pageJson = new PageJson<UserInfo>();
                pageJson.Items = userService.GetUserGrid(query);
                pageJson.TotalCount = query.TotalCount;
                resultModel.data = pageJson;
            }
            catch (Exception ex)
            {
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.ServerError.ToString());
                resultModel.success = false;
                resultModel.message = ex.Message;
            }
            return resultModel;
        }
        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("userinfo")]
        [HttpGet]
        public async Task<ResultModel<UserInfo>> GetUserInfo(int id)
        {
            ResultModel<UserInfo> resultModel = new ResultModel<UserInfo>();
            try
            {
                resultModel.data = await userService.GetUserById(id);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 头像
        /// </summary>
        /// <param name="file">文件</param>
        /// <returns></returns>
        [Route("headimg")]
        [HttpPost]
        public ResultModel<string> HeadImg(IFormFile file)
        {
            ResultModel<string> resultModel = new ResultModel<string>();
            string fileUrls = "";
            try
            {
                if (file != null)
                {
                    string url = "User/HeadImg/" + file.FileName;
                    string saveFilePath = hostingEnvironment.ContentRootPath;
                    var fileFolder = Path.Combine(saveFilePath, "HeadImg");
                    if (!Directory.Exists(fileFolder))
                    {
                        Directory.CreateDirectory(fileFolder);
                    }
                    var fileNamelocal = DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(file.FileName);
                    var filePath = Path.Combine(fileFolder, fileNamelocal);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            resultModel.data = fileUrls;
            return resultModel;
        }
        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <param name="modifyUser">用户实体</param>
        /// <returns></returns>
        [Route("modify")]
        [HttpPost]
        public async Task<ResultModel<bool>> ModifyUser(ModifyUserDto modifyUser)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                if (userService.Entities().Any(zw => zw.UserName == modifyUser.UserName && !zw.IsDelete && zw.Id != modifyUser.Id))
                {
                    resultModel.success = false;
                    resultModel.message = "用户名已存在";
                    resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
                }
                else
                {
                    modifyUser.UpdateUser = UserName;
                    modifyUser.UpdateTime = DateTime.Now;
                    resultModel.data = await userService.EditUser(modifyUser);
                }
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }

        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <param name="modifyUser">用户实体</param>
        /// <returns></returns>
        [Route("modify-user")]
        [HttpPost]
        public async Task<ResultModel<bool>> ModifyUserBy(UpdateUserDto modifyUser)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                if (userService.Entities().Any(zw => zw.UserName == modifyUser.UserName && !zw.IsDelete && zw.Id != modifyUser.Id))
                {
                    resultModel.success = false;
                    resultModel.message = "用户名已存在";
                    resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
                }
                //string oldPassword = EncryptHelper.GetMD5_32(modifyUser.Password);
                //var user = userService.Entities().FirstOrDefault(zw => zw.Id == modifyUser.Id);
                //if (user != null && user.Password.ToLower() != modifyUser.Password.ToLower())
                //{
                //    resultModel.success = false;
                //    resultModel.message = "原密码错误";
                //    resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
                //}
                //else
                //{
                modifyUser.Password = EncryptHelper.GetMD5_32(modifyUser.Password);
                modifyUser.UpdateUser = UserName;
                modifyUser.UpdateTime = DateTime.Now;
                resultModel.data = await userService.UpdateUser(modifyUser);
                // }
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }


        ///// <summary>
        ///// 角色
        ///// </summary>
        ///// <returns></returns>
        //[Route("role")]
        //[HttpGet]
        //public ResultModels<TextValue> GetRole()
        //{
        //    ResultModels<TextValue> resultModel = new ResultModels<TextValue>();
        //    resultModel.data = new List<TextValue>() { new TextValue() { Value = "-1", Text = "搜索类型" } };
        //    List<Role> roles = roleService.GetAllList(zw => !zw.IsDelete);
        //    resultModel.data.AddRange(roles.Select(zw => new TextValue() { Text = zw.Name, Value = zw.Id.ToString() }).ToList());
        //    return resultModel;
        //}

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns></returns>
        [Route("delete")]
        [HttpPost]
        public async Task<ResultModel<bool>> DeleteAsync(int id)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                resultModel.data = await userService.DeleteByUserIdAsync(id);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 获取用户权限
        /// </summary> 
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        [Route("permission")]
        [HttpGet]
        public ResultModel<List<int>> GetUserPermission(int userId)
        {
            ResultModel<List<int>> resultModel = new ResultModel<List<int>>();
            var userPermissions = userPermissionService.Entities().Where(zw => zw.UserId == userId).ToList();
            if (userPermissions != null & userPermissions.Count > 0)
            {
                resultModel.data = userPermissions.Select(zw => zw.PermissionId).ToList();
            }
            return resultModel;
        }
        /// <summary>
        /// 设置权限
        /// </summary>
        /// <param name="model">权限</param>
        /// <returns></returns>
        [Route("setpermission")]
        [HttpPost]
        [UnitOfWork]
        public async Task<ResultModel<bool>> SetUserPermission(PermissionModel permissionModel)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                var useruserPermissions = userPermissionService.Entities().Where(zw => zw.UserId == permissionModel.model.UserId).Select(zw => zw.Id).ToList();
                if (useruserPermissions.Count > 0)
                {
                    await userPermissionService.BlukDeleteAsync(useruserPermissions);
                }
                List<UserPermission> userPermissions = permissionModel.model.PermissionIds.Distinct().Select(zw =>
                new UserPermission()
                {
                    PermissionId = zw,
                    UserId = permissionModel.model.UserId,
                    CreateTime = DateTime.Now,
                    CreateUser = UserName,
                    IsDelete = false
                }
                ).ToList();
                await userPermissionService.BlukInsertAsync(userPermissions);
                resultModel.data = true;
                resultModel.code = "200";
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
    }
}
