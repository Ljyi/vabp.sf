﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.System;
using VAbp.SF.System.Query;

namespace VAbp.SF.Controllers
{
    /// <summary>
    /// 菜单Iocn
    /// </summary>
    [Route("api/v1/system/menuicon")]
    [ApiExplorerSettings(GroupName = "V1")]
    public class MenuIconController : BaseController
    {
        private readonly IMenuIconService menuIconService;
        public MenuIconController(IMenuIconService menuIconService)
        {
            this.menuIconService = menuIconService;
        }
        /// <summary>
        /// 获取菜单Iocn（分页）
        /// </summary>
        /// <returns></returns>
        [Route("Query")]
        [HttpPost]
        public ResultPage<MenuIconInfo> GetMenuIconQuery(MenuIconQuery menuIconQuery)
        {
            ResultPage<MenuIconInfo> resultModel = new ResultPage<MenuIconInfo>();
            try
            {
                PageJson<MenuIconInfo> pageJson = new PageJson<MenuIconInfo>();
                PageParameter pg = menuIconQuery;
                pageJson.Items = menuIconService.GetMenuIconGrid(menuIconQuery);
                pageJson.TotalCount = pg.TotalCount;
                resultModel.data = pageJson;
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 获取菜单Iocn（全部）
        /// </summary>
        /// <returns></returns>
        [Route("list")]
        [HttpGet]
        public ResultModel<List<MenuIconDto>> GetMenuIconList()
        {
            ResultModel<List<MenuIconDto>> resultModel = new ResultModel<List<MenuIconDto>>();
            try
            {
                resultModel.data = menuIconService.GetMenuIconAll();
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
    }
}
