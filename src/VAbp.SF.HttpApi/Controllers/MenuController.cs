﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.Models;
using VAbp.SF.Models.Test;
using VAbp.SF.System;
using VAbp.SF.System.Query;
using Volo.Abp.Authorization;

namespace VAbp.SF.Controllers
{
    /// <summary>
    /// 菜单
    /// </summary>
    [Route("api/v1/system/menu")]
    [ApiExplorerSettings(GroupName = "V1")]
    public class MenuController : BaseController
    {
        IMenuService _menuService;
        public MenuController(IMenuService menuService)
        {
            this._menuService = menuService;
        }
        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("menuinfo")]
        [HttpGet]
        public async Task<ResultModel<MenuInfo>> GetMenuInfo(int id)
        {
            ResultModel<MenuInfo> resultModel = new ResultModel<MenuInfo>();
            try
            {
                var menuList = _menuService.Entities().Where(s => !s.IsDelete);
                var result = await _menuService.GetMenuById(id);
                result.ParentList = new List<int>() { 0 };
                if (result != null && result.ParentId != 0)
                {
                    List<int> resultList = new List<int>();
                    result.ParentList = GetParents(result.ParentId, menuList, resultList);
                    resultList.Insert(0, 0);
                    result.ParentList = resultList;
                }

                resultModel.data = result;
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 获取菜单
        /// </summary>
        /// <returns></returns>
        [Route("menu")]
        [HttpGet]
        public ResultModel<List<MenuInfo>> GetMenus()
        {
            ResultModel<List<MenuInfo>> resultModel = new ResultModel<List<MenuInfo>>();
            try
            {
                resultModel.data = _menuService.GetMenuAll();
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 获取菜单
        /// </summary>
        /// <returns></returns>
        [Route("list")]
        [HttpPost]
        public ResultPage<MenuInfo> GetMenuList(MenuQuery menuPage)
        {
            ResultPage<MenuInfo> resultModel = new ResultPage<MenuInfo>();
            try
            {
                PageJson<MenuInfo> pageJson = new PageJson<MenuInfo>();
                PageParameter pg = menuPage;
                pageJson.Items = _menuService.GetMenuGrid(menuPage);
                pageJson.TotalCount = pg.TotalCount;
                resultModel.data = pageJson;
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 菜单树
        /// </summary>
        /// <returns></returns>
        [Route("tree")]
        [HttpGet]
        public ResultModel<List<MenuTree>> MenuTree(string selected)
        {
            ResultModel<List<MenuTree>> resultModel = new ResultModel<List<MenuTree>>();
            try
            {
                var MenuTrees = _menuService.GetMenuAll();
                var temp = MenuTrees.Select(x => new MenuTree
                {
                    Id = x.Id,
                    ParentId = x.ParentId,
                    Title = x.MenuName
                }).ToList();
                var root = new MenuTree
                {
                    Title = "顶级菜单",
                    Id = 0,
                    ParentId = null
                };
                temp.Insert(0, root);
                resultModel.data = temp.BuildMenuTree(int.Parse(selected));
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 创建菜单
        /// </summary>
        /// <returns></returns>
        [Route("create")]
        [HttpPost]
        public async Task<ResultModel<bool>> CreateMenu(AddMenuDto addMenuDto)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                addMenuDto.CreateUser = UserName;
                addMenuDto.CreateTime = DateTime.Now;
                resultModel.data = await _menuService.AddMenu(addMenuDto);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 修改菜单
        /// </summary>
        /// <returns></returns>
        [Route("modify")]
        [HttpPost]
        public async Task<ResultModel<bool>> ModifyMenu(EditMenuDto editMenuDto)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                editMenuDto.UpdateUser = UserName;
                editMenuDto.UpdateTime = DateTime.Now;
                resultModel.data = await _menuService.EditMenu(editMenuDto);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <returns></returns>
        [Route("delete")]
        [HttpPost]
        public async Task<ResultModel<bool>> DeleteMenu(int id)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                resultModel.data = await _menuService.DeleteAsync(id, UserName);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }

        /// <summary>
        /// 递归查询父级ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="list"></param>
        /// <param name="resultList"></param>
        /// <returns></returns>
        private List<int> GetParents(int id, IQueryable<Menu> list, List<int> resultList)
        {
            resultList.Insert(0, id);
            Menu result = list.Where(x => x.Id == id).FirstOrDefault();
            if (result != null && result.ParentId != 0)
            {
                return GetParents(result.ParentId, list, resultList);
            }
            else
            {
                return resultList;
            }
        }
    }
}
