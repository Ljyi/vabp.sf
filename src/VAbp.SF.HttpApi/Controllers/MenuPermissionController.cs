﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.Models;
using VAbp.SF.System;
using VAbp.SF.System.Query;

namespace VAbp.SF.Controllers
{
    /// <summary>
    /// 菜单权限
    /// </summary>
    [Route("api/v1/system/menupermission")]
    [ApiExplorerSettings(GroupName = "V1")]
    [ApiController]
    public class MenuPermissionController : BaseController
    {
        IMenuService menuService;
        IPermissionService permissionService;
        public MenuPermissionController(IMenuService menuService, IPermissionService permissionService)
        {
            this.permissionService = permissionService;
            this.menuService = menuService;
        }

        /// <summary>
        /// 权限列表
        /// </summary>
        /// <param name="permissionQuery">查询</param>
        /// <returns></returns>
        [Route("query")]
        [HttpPost]
        public ResultPage<PermissionInfo> GetMenuPermissionGrid(PermissionQuery permissionQuery)
        {
            ResultPage<PermissionInfo> resultModel = new ResultPage<PermissionInfo>();
            try
            {
                PageJson<PermissionInfo> pageJson = new PageJson<PermissionInfo>();
                pageJson.Items = permissionService.GetPermissionGrid(permissionQuery);
                pageJson.TotalCount = permissionQuery.TotalCount;
                resultModel.data = pageJson;
            }
            catch (Exception ex)
            {
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.ServerError.ToString());
                resultModel.success = false;
                resultModel.message = ex.Message;
            }
            return resultModel;
        }

        /// <summary>
        /// 菜单权限
        /// </summary>
        /// <returns></returns>
        [Route("all")]
        [HttpGet]
        public ResultModel<List<MenuPermission>> GetMenuPermissions()
        {
            ResultModel<List<MenuPermission>> resultModel = new ResultModel<List<MenuPermission>>();
            try
            {
                List<Permission> lists = permissionService.Entities().Where(zw => !zw.IsDelete).ToList();
                List<MenuPermission> menuPermissions = new List<MenuPermission>();
                foreach (var item in lists.GroupBy(zw => zw.MenuId))
                {
                    MenuPermission menuPermission = new MenuPermission() { MenuId = item.Key, MenuName = item.First().Menu.MenuName };
                    menuPermission.Permissions = item.Select(zw => new PermissionItem() { Id = zw.Id, MenuId = zw.MenuId, Name = zw.Name }).ToList();
                    menuPermissions.Add(menuPermission);
                }
                resultModel.data = menuPermissions;
            }
            catch (Exception ex)
            {
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.ServerError.ToString());
                resultModel.success = false;
                resultModel.message = ex.Message;
            }
            return resultModel;
        }
        /// <summary>
        /// 菜单权限
        /// </summary>
        /// <returns></returns>
        [Route("menupermissiontree")]
        [HttpGet]
        public ResultModel<List<MenuPermissionTree>> GetMenuPermissionTree()
        {
            ResultModel<List<MenuPermissionTree>> resultModel = new ResultModel<List<MenuPermissionTree>>();
            try
            {
                //菜单
                List<MenuPermissionTree> menuPermissionTrees = new List<MenuPermissionTree>();
                List<Menu> menus = menuService.Entities().Where(zw => !zw.IsDelete).ToList();
                var menusParent = menus.Where(zw => zw.ParentId == 0).ToList();
                //权限
                List<Permission> Permissions = permissionService.Entities().Where(zw => !zw.IsDelete).ToList();
                foreach (var item in menusParent)
                {
                    var permission = Permissions.FirstOrDefault(zw => zw.Status == 1 && zw.Type == 0 && !zw.IsDelete && zw.MenuId == item.Id);
                    if (permission != null)
                    {
                        MenuPermissionTree menuPermissionTree = new MenuPermissionTree() { Child = new List<MenuPermissionTree>() };
                        menuPermissionTree.MenuId = item.Id;
                        menuPermissionTree.MenuName = item.MenuName;
                        menuPermissionTree.PermissionId = permission.Id;
                        menuPermissionTree.Type = 0;
                        GetChildMenu(menuPermissionTree, item.Id);
                        menuPermissionTrees.Add(menuPermissionTree);
                    }
                }
                resultModel.data = menuPermissionTrees;
            }
            catch (Exception ex)
            {
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.ServerError.ToString());
                resultModel.success = false;
                resultModel.message = ex.Message;
            }
            return resultModel;
        }
        /// <summary>
        /// 递归获取子节点数据
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="parentId"></param>
        private void GetChildMenu(MenuPermissionTree tree, int parentId)
        {
            var allmenus = menuService.Entities().Where(zw => !zw.IsDelete).ToList();
            var menus = allmenus.Where(zw => zw.ParentId == parentId);
            foreach (var item in menus)
            {
                MenuPermissionTree permissionTree = new MenuPermissionTree() { Child = new List<MenuPermissionTree>() };
                permissionTree.MenuName = item.MenuName;
                permissionTree.MenuId = item.Id;
                permissionTree.Type = 0;
                Permission permission = permissionService.Entities().Where(zw => !zw.IsDelete && zw.MenuId == item.Id && zw.Type == 0).FirstOrDefault();
                if (permission != null && permission.Type == 0)
                {
                    permissionTree.PermissionId = permission.Id;
                }
                if (allmenus.Any(zw => zw.ParentId == item.Id))
                {
                    GetChildMenu(permissionTree, item.Id);
                }
                else
                {
                    List<Permission> Permissions = permissionService.Entities().Where(zw => !zw.IsDelete && zw.MenuId == item.Id).ToList();
                    foreach (var itemPermission in Permissions)
                    {
                        MenuPermissionTree permissionTree1 = new MenuPermissionTree();
                        permissionTree1.MenuName = item.MenuName;
                        permissionTree1.MenuId = item.Id;
                        permissionTree1.PermissionId = itemPermission.Id;
                        permissionTree1.PermissionName = itemPermission.Name;
                        permissionTree1.Type = 1;
                        permissionTree.Child.Add(permissionTree1);
                    }
                }
                tree.Child.Add(permissionTree);
            }
        }
        /// <summary>
        /// 获取权限信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("info")]
        [HttpGet]
        public async Task<ResultModel<PermissionInfo>> GetMenuPermissionInfo(int id)
        {
            ResultModel<PermissionInfo> resultModel = new ResultModel<PermissionInfo>();
            try
            {
                resultModel.data = await permissionService.GetPermissionById(id);
                var result = await menuService.GetMenuById(resultModel.data.MenuId);
                resultModel.data.MeunList = new List<int>() { 0, result.Id };
                if (result != null && result.ParentId != 0)
                {
                    var menuList = menuService.Entities().Where(s => !s.IsDelete);
                    List<int> resultList = new List<int>();
                    result.ParentList = GetParents(result.ParentId, menuList, resultList);
                    resultList.Insert(0, 0);
                    resultList.Add(result.Id);
                    resultModel.data.MeunList = resultList;
                }
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 创建菜单权限
        /// </summary>
        /// <param name="addPermission">菜单权限实体</param>
        /// <returns></returns>
        [Route("add")]
        [HttpPost]
        public async Task<ResultModel<bool>> AddMenuPermission(AddPermissionDto addPermission)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                if (permissionService.Entities().Any(zw => zw.MenuId == addPermission.MenuId && zw.ActionCode == addPermission.ActionCode && zw.Type == 0))
                {
                    resultModel.success = false;
                    resultModel.message = "菜单权限已存在";
                    resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
                }
                else
                {
                    addPermission.CreateUser = UserName;
                    addPermission.CreateTime = DateTime.Now;
                    resultModel.data = await permissionService.AddPermission(addPermission);
                }
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 编辑菜单权限
        /// </summary>
        /// <param name="editPermission">菜单权限实体</param>
        /// <returns></returns>
        [Route("modify")]
        [HttpPost]
        public async Task<ResultModel<bool>> ModifyMenuPermission(EditPermissionDto editPermission)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                editPermission.UpdateUser = UserName;
                editPermission.UpdateTime = DateTime.Now;
                resultModel.data = await permissionService.EditPermission(editPermission);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 删除菜单权限
        /// </summary>
        /// <param name="id">菜单权限id</param>
        /// <returns></returns>
        [Route("delete")]
        [HttpPost]
        public ResultModel<bool> Delete(int id)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                //var per = permissionService.FindById(id);
                //per.IsDelete = true;
                //per.UpdateTime = DateTime.Now;
                //per.UpdateUser = UserName;
                //resultModel.data = await permissionService.UpdateAsync(per);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 递归查询父级ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="list"></param>
        /// <param name="resultList"></param>
        /// <returns></returns>
        private List<int> GetParents(int id, IQueryable<Menu> list, List<int> resultList)
        {
            resultList.Insert(0, id);
            Menu result = list.Where(x => x.Id == id).FirstOrDefault();
            if (result != null && result.ParentId != 0)
            {
                return GetParents(result.ParentId, list, resultList);
            }
            else
            {
                return resultList;
            }
        }
    }
}
