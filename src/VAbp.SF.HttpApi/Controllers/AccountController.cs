﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.Models;
using VAbp.SF.System;

namespace VAbp.SF.Controllers
{
    /// <summary>
    /// 账号权限
    /// </summary>
    [Route("api/v1/account")]
    [ApiExplorerSettings(GroupName = "V1")]
    [ApiController]
    public class AccountController : BaseController
    {
        IPermissionService permissionService;
        IUserService userService;
        IUserPermissionService userPermissionService;
        public AccountController(IUserService userService, IPermissionService permissionService, IUserPermissionService userPermissionService)
        {
            this.userPermissionService = userPermissionService;
            this.permissionService = permissionService;
            this.userService = userService;
        }
        /// <summary>
        /// 获取权限
        /// </summary>
        /// <returns></returns>
        [Route("profile")]
        [HttpGet]
        public async Task<ResultModel<Models.UserPermissions>> GetUserPermissions()
        {
            ResultModel<Models.UserPermissions> resultModel = new ResultModel<Models.UserPermissions>();
            if (UserId.HasValue)
            {
                int userId = UserId.Value;
                UserInfo userInfo = await userService.GetUserById(userId);
                List<int> userpList = userPermissionService.Entities().Where(s => !s.IsDelete && s.UserId == userId).Select(s => s.PermissionId).ToList();
                List<Permission> permissionInfos = new List<Permission>();
                if (userpList != null)
                {
                    permissionInfos = permissionService.Entities().Where(s => !s.IsDelete && userpList.Contains(s.Id) && s.Type != 0).ToList();
                }
                var pagePermissions = permissionInfos.Select(s => s.ActionCode).ToList();//GroupBy(x => x.MenuAlias.ToString()).ToDictionary(g => g.Key, g => g.Select(x => x.ActionCode).Distinct().ToList());
                Models.UserPermissions userPermission = new Models.UserPermissions();
                userPermission.Avatar = userInfo.HeadImg;
                userPermission.UserId = userId;
                userPermission.UserName = userInfo.UserName;
                if (userInfo.IsAdmin)
                {
                    userPermission.UserType = 1;
                }
                else
                {
                    userPermission.UserType = 0;
                }
                userPermission.Permissions = pagePermissions;
                resultModel.data = userPermission;
            }
            else
            {
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
                resultModel.success = false;
                resultModel.message = "请重新登录";
            }
            return resultModel;
        }
        /// <summary>
        /// 加载菜单
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("menu")]
        public List<MenuItem> Menu()
        {
            List<MenuItem> resultModel = new List<MenuItem>();
            if (UserId.HasValue)
            {
                int userId = UserId.Value;
                try
                {
                    List<MenuInfo> menuInfos = userPermissionService.GetUserMenus(userId, IsAdmin);
                    menuInfos = menuInfos.OrderBy(x => x.Sort).ThenBy(x => x.CreateTime).ToList();
                    return MenuItemHelper.LoadMenuTree(menuInfos, "0");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return resultModel;
        }
    }
}
