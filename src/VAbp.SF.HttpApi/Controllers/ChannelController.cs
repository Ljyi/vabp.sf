﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.System;
using VAbp.SF.System.Query;

namespace VAbp.SF.Controllers
{
    /// <summary>
    /// 渠道
    /// </summary>
    [Route("api/v1/channel")]
    [ApiExplorerSettings(GroupName = "V1")]
    public class ChannelController : BaseController
    {
        IChannelService _channelService;
        public ChannelController(IChannelService channelService)
        {
            this._channelService = channelService;
        }
        /// <summary>
        /// 获取渠道信息（订单查询）
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("select")]
        [HttpGet]
        public ResultModel<List<ChannelTree>> GetUserChannelTree()
        {
            ResultModel<List<ChannelTree>> resultModel = new ResultModel<List<ChannelTree>>();
            try
            {
                if (IsAdmin)
                {
                    resultModel.data = _channelService.GetChannelTree();
                }
                else
                {
                    resultModel.data = _channelService.GetUserChannelTree(UserId.Value);
                }
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }

        /// <summary>
        /// 获取渠道信息（顶级菜单）
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("select-tree")]
        [HttpGet]
        public ResultModel<ChannelTree> GetselectChannelTree()
        {
            ResultModel<ChannelTree> resultModel = new ResultModel<ChannelTree>();
            try
            {
                resultModel.data = _channelService.GetSelectChannelTree();
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 获取渠道信息（渠道树）
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("tree")]
        [HttpGet]
        public ResultModel<List<ChannelTree>> GetChannelTree()
        {
            ResultModel<List<ChannelTree>> resultModel = new ResultModel<List<ChannelTree>>();
            try
            {
                resultModel.data = _channelService.GetChannelTree();
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 获取渠道信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("info")]
        [HttpGet]
        public async Task<ResultModel<ChannelDto>> GetChannelInfo(int id)
        {
            ResultModel<ChannelDto> resultModel = new ResultModel<ChannelDto>();
            try
            {
                var menuList = _channelService.Entities().Where(s => !s.IsDelete);
                var result = await _channelService.GetChannelById(id);
                resultModel.data = result;
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 获取渠道
        /// </summary>
        /// <returns></returns>
        [Route("query")]
        [HttpPost]
        public ResultPage<ChannelInfo> GetChannelGrid(ChannelQuery channelQuery)
        {
            ResultPage<ChannelInfo> resultModel = new ResultPage<ChannelInfo>();
            try
            {
                PageJson<ChannelInfo> pageJson = new PageJson<ChannelInfo>();
                PageParameter pg = channelQuery;
                pageJson.Items = _channelService.GetChannelGrid(channelQuery);
                pageJson.TotalCount = pg.TotalCount;
                resultModel.data = pageJson;
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 创建渠道
        /// </summary>
        /// <returns></returns>
        [Route("add")]
        [HttpPost]
        public async Task<ResultModel<bool>> CreateChannel(AddChannelDto addChannelDto)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                addChannelDto.CreateUser = UserName;
                addChannelDto.CreateTime = DateTime.Now;
                resultModel.data = await _channelService.AddChannel(addChannelDto);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 修改渠道
        /// </summary>
        /// <returns></returns>
        [Route("modify")]
        [HttpPost]
        public async Task<ResultModel<bool>> ModifyChannel(EditChannelDto editChannelDto)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                editChannelDto.UpdateUser = UserName;
                editChannelDto.UpdateTime = DateTime.Now;
                resultModel.data = await _channelService.EditChannel(editChannelDto);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <returns></returns>
        [Route("delete")]
        [HttpPost]
        public async Task<ResultModel<bool>> DeleteChannel(int id)
        {
            ResultModel<bool> resultModel = new ResultModel<bool>();
            try
            {
                resultModel.data = await _channelService.DeleteAsync(id, UserName);
            }
            catch (Exception ex)
            {
                resultModel.success = false;
                resultModel.message = ex.Message;
                resultModel.code = EnumExtension.GetEnumValue(typeof(ResultCode), ResultCode.RequestValidateFail.ToString());
            }
            return resultModel;
        }
    }
}
