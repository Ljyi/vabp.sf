﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.Models
{
    /// <summary>
    /// 用户权限
    /// </summary>
    public class UserPermissions
    {
        /// <summary>
        /// 
        /// </summary>
        public string[] access { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 是否超级管理员
        /// </summary>
        public int UserType { get; set; }
        /// <summary>
        /// 权限
        /// </summary>
        public List<string> Permissions { get; set; }
    }
}
