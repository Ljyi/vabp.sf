﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.Models
{
    public class OrderExport
    {
        public string 订单号 { get; set; }
        public string 姓名 { get; set; }
        public string 手机号 { get; set; }
        public string 地址 { get; set; }
        public string 备注 { get; set; }
        public string 日期 { get; set; }
        public string 时间 { get; set; }
    }
}
