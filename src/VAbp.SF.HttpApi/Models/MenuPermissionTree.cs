﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.Models
{
    /// <summary>
    /// 菜单树
    /// </summary>
    public class MenuPermissionTree
    {
        public MenuPermissionTree()
        {
            Child = new List<MenuPermissionTree>();
        }
        /// <summary>
        /// 菜单Id
        /// </summary>
        public int MenuId { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// 权限名称
        /// </summary>
        public string PermissionName { get; set; }
        /// <summary>
        /// 类型： 0：菜单，1：按钮
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 权限Id
        /// </summary>
        public int PermissionId { get; set; }
        /// <summary>
        /// 子节点
        /// </summary>
        public List<MenuPermissionTree> Child { get; set; }
    }

    /// <summary>
    /// 菜单权限
    /// </summary>
    public class MenuPermission
    {
        /// <summary>
        /// 菜单Id
        /// </summary>
        public int MenuId { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// 权限Id
        /// </summary>
        public int PermissionId { get; set; }
        /// <summary>
        /// 操作权限集合
        /// </summary>
        public List<PermissionItem> Permissions { get; set; }
    }
    /// <summary>
    /// 操作权限
    /// </summary>
    public class PermissionItem
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 权限编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 菜单Id
        /// </summary>
        public int MenuId { get; set; }
        /// <summary>
        /// 菜单别名(与前端路由配置中的name值保持一致)
        /// </summary>
        public string MenuAlias { get; set; }
        /// <summary>
        /// 权限名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 权限操作码
        /// </summary>
        public string ActionCode { get; set; }
    }
}
