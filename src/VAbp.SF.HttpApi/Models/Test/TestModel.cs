﻿using System;

namespace VAbp.SF.Models.Test
{
    public class TestModel
    {
        public string Name { get; set; }

        public DateTime BirthDate { get; set; }
    }
}