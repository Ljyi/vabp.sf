﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VAbp.SF.Models
{
    /// <summary>
    /// 登录
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// 账号
        /// </summary>
        [Required(ErrorMessage = "请输入有效Account")]
        public string Account { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [Required]
        [StringLength(33, MinimumLength = 6, ErrorMessage = "密码字符长度在6-33之间")]
        public string Password { get; set; }
    }
}
