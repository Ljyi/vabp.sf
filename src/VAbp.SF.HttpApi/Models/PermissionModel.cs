﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.Models
{

    public class PermissionModel
    {
        public SetPermissionModel model { get; set; }
    }
    /// <summary>
    /// 设置权限
    /// </summary>
    public class SetPermissionModel
    {
        /// <summary>
        /// 权限Id
        /// </summary>
        public List<int> PermissionIds { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
    }
    public class UserChanelModel
    {
        public SetUserChanelModel model { get; set; }
    }
    /// <summary>
    /// 设置权限
    /// </summary>
    public class SetUserChanelModel
    {
        /// <summary>
        /// 权限Id
        /// </summary>
        public List<int> ChanelIds { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
    }
}
