﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.System;
using Volo.Abp.Authorization;

namespace VAbp.SF.Auth.Policys
{
    /// <summary>
    /// 权限授权处理器 继承AuthorizationHandler ，并且需要一个权限必要参数
    /// </summary>
    public class PermissionHandler : AuthorizationHandler<PermissionRequirementM>
    {
        /// <summary>
        /// 验证方案提供对象
        /// </summary>
        public IAuthenticationSchemeProvider Schemes { get; set; }
        /// <summary>
        /// 自定义策略参数
        /// </summary>
        public PermissionRequirementM Requirement { get; set; }
        private readonly IHttpContextAccessor _accessor;
        IUserService userService;
        /// <summary>
        /// jwt 服务
        /// </summary>
        // private IJwtAppCacheService _jwtApp;
        // IPooledRedisManagerServcie pooledRedisManagerServcie;
        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="schemes"></param>
        public PermissionHandler(IAuthenticationSchemeProvider schemes, IUserService userService, IHttpContextAccessor accessor)//,IJwtAppCacheService jwtApp, IPooledRedisManagerServcie pooledRedisManagerServcie
        {
            _accessor = accessor;
            Schemes = schemes;
            this.userService = userService;
            //_jwtApp = jwtApp;
            //this.pooledRedisManagerServcie = pooledRedisManagerServcie;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requirement"></param>
        /// <returns></returns>
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirementM requirement)
        {
            //从AuthorizationHandlerContext转成HttpContext，以便取出表求信息
            var filterContext = (context.Resource as Microsoft.AspNetCore.Mvc.Filters.AuthorizationFilterContext);
            var httpContext = (context.Resource as Microsoft.AspNetCore.Mvc.Filters.AuthorizationFilterContext)?.HttpContext;
            if (httpContext == null)
            {
                httpContext = _accessor.HttpContext;
            }
            //请求Url
            var questUrl = httpContext.Request.Path.Value.ToLower();
            //判断请求是否停止
            var handlers = httpContext.RequestServices.GetRequiredService<IAuthenticationHandlerProvider>();
            foreach (var scheme in await Schemes.GetRequestHandlerSchemesAsync())
            {
                var handler = await handlers.GetHandlerAsync(httpContext, scheme.Name) as IAuthenticationRequestHandler;
                if (handler != null && await handler.HandleRequestAsync())
                {
                    context.Fail();
                    return;
                }
            }
            //判断请求是否拥有凭据，即有没有登录
            var defaultAuthenticate = await Schemes.GetDefaultAuthenticateSchemeAsync();
            if (defaultAuthenticate != null)
            {
                var result = await httpContext.AuthenticateAsync(defaultAuthenticate.Name);
                if (result?.Principal != null && result.Succeeded)
                {
                    httpContext.User = result.Principal;
                    //判断是否为已停用的 Token
                    string userId = httpContext.User.Claims.SingleOrDefault(s => s.Type == ClaimTypes.Sid).Value;
                    string token = httpContext.Request.Headers["Authorization"].Single().Split(" ").Last();
                    //string redisToken = pooledRedisManagerServcie.Get<string>(userId, RedisDB.Default05);
                    //if (redisToken != token)
                    //{
                    //    context.Fail();
                    //    return;
                    //}
                    var userInfo = await userService.GetUserById(int.Parse(userId));
                    if (userInfo != null)
                    {
                        string Password = httpContext.User.Claims.SingleOrDefault(s => s.Type == "Password").Value;
                        if (userInfo.Password.ToUpper() != Password.ToUpper())
                        {
                            context.Fail();
                            return;
                        }
                    }
                    else
                    {
                        context.Fail();
                        return;
                    }
                    //权限中是否存在请求的url
                    if (requirement.Permissions.GroupBy(g => g.Url).Where(w => w.Key.ToLower() == questUrl).Count() > 0)
                    {
                        // var name = httpContext.User.Claims.SingleOrDefault(s => s.Type == requirement.ClaimType).Value;
                        //验证权限
                        if (requirement.Permissions.Where(w => w.UserId == int.Parse(userId) && w.Url.ToLower() == questUrl).Count() <= 0)
                        {
                            //无权限跳转到拒绝页面
                            httpContext.Response.Redirect(requirement.DeniedAction);
                        }
                    }
                    //判断过期时间
                    try
                    {
                        if (DateTime.Parse(httpContext.User.Claims.SingleOrDefault(s => s.Type == ClaimTypes.Expiration).Value) >= DateTime.Now)
                        {
                            context.Succeed(requirement);
                        }
                        else
                        {
                            //  pooledRedisManagerServcie.Remove(userId, RedisDB.Default05);
                            context.Fail();
                        }
                    }
                    catch (Exception)
                    {
                        context.Succeed(requirement);
                    }
                    return;
                }
            }
            //判断没有登录时，是否访问登录的url,并且是Post请求，并且是form表单提交类型，否则为失败
            if (!questUrl.Equals(requirement.LoginPath.ToLower(), StringComparison.Ordinal) && (!httpContext.Request.Method.Equals("POST") || !httpContext.Request.HasFormContentType))
            {
                context.Fail();
                return;
            }
            context.Succeed(requirement);
        }
        // }
    }
}
