﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using VAbp.SF.Auth.Jwt;
using Volo.Abp.Authorization;

namespace VAbp.SF.Auth.Policys
{
    public class JwtToken
    {
        /// <summary>
        /// 获取基于JWT的Token
        /// </summary>
        /// <param name="claims"></param>
        /// <param name="permissionRequirement"></param>
        /// <returns></returns>
        public static TokenInfo BuildJwtToken(Claim[] claims, PermissionRequirementM permissionRequirement)
        {
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: permissionRequirement.Issuer,
                audience: permissionRequirement.Audience,
                claims: claims,
                notBefore: now,
                expires: now.Add(permissionRequirement.Expiration),
                signingCredentials: permissionRequirement.SigningCredentials
            );
            // 生成 Token
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            var response = new TokenInfo
            {
                Success = true,
                Access_Token = encodedJwt,
                Expires_In = permissionRequirement.Expiration.TotalMilliseconds,
                Token_Type = "Bearer"
            };
            return response;
        }
    }
}
