﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.Auth
{
    /// <summary>
    /// Jwt配置
    /// </summary>
    public class JwtSetting
    {
        /// <summary>
        /// key
        /// </summary>
        public string SecurityKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Audience { get; set; }

        public double Expires { get; set; }
    }
}
