﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.Auth.Jwt
{
    public class TokenInfo
    {
        /// <summary>
        /// 状态 是否成功
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// Token
        /// </summary>
        public string Access_Token { get; set; }
        /// <summary>
        /// 有效时长，单位秒
        /// </summary>
        public double Expires_In { get; set; }
        /// <summary>
        /// Token 类型
        /// </summary>
        public string Token_Type { get; set; }
    }
}
