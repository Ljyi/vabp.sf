﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.Auth
{
    /// <summary>
    /// 配置
    /// </summary>
    public class ConfigSettings
    {
        /// <summary>
        /// 默认加密Key
        /// </summary>
        public string PwdKey { get; set; }
        /// <summary>
        /// jwt配置
        /// </summary>
        public JwtSetting JwtSetting { get; set; }
    }
}
