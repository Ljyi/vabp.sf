﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.Auth
{
    /// <summary>
    /// 令牌
    /// </summary>
    public class TokenModelJwt
    {
        /// <summary>
        /// Uid
        /// </summary>
        public int Uid { get; set; }
        /// <summary>
        /// 角色
        /// </summary>
        public string Role { get; set; }
        /// <summary>
        /// 职能
        /// </summary>
        public string Work { get; set; }
        /// <summary>
        /// 有效时间（秒）
        /// </summary>
        public int Expires_In { get; set; }
    }
}
