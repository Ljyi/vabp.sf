﻿using Volo.Abp.Account;
using Volo.Abp.AutoMapper;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Identity;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement;
using Volo.Abp.TenantManagement;

namespace VAbp.SF
{
    [DependsOn(
        typeof(SFDomainModule),
        // typeof(AbpAccountApplicationModule),
        typeof(AbpAutoMapperModule),
        typeof(SFApplicationContractsModule)
        //typeof(AbpIdentityApplicationModule),
        //typeof(AbpPermissionManagementApplicationModule),
        //typeof(AbpTenantManagementApplicationModule),
        //typeof(AbpFeatureManagementApplicationModule)
        )]
    public class SFApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<SFApplicationModule>();
                //options.AddProfile<SFApplicationAutoMapperProfile>(validate: true);
            });
        }
    }
}
