﻿using System;
using System.Collections.Generic;
using System.Text;
using VAbp.SF.Localization;
using Volo.Abp.Application.Services;

namespace VAbp.SF
{
    /* Inherit your application services from this class.
     */
    public abstract class ServiceBase : ApplicationService
    {
        protected ServiceBase()
        {
            LocalizationResource = typeof(SFResource);
        }
    }
}
