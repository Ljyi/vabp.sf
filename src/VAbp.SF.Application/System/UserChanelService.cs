﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.System.Repository;

namespace VAbp.SF.System
{
    public class UserChanelService : ServiceBase, IUserChanelService
    {
        private readonly IUserChanelRepository _userChanelRepository;
        public UserChanelService(IUserChanelRepository userChanelRepository)
        {
            _userChanelRepository = userChanelRepository;
        }
        public List<int> GetUserChanelByUserId(int userId)
        {
            List<int> chanelIds = new List<int>();
            Expression<Func<UserChanel, bool>> chanelex = t => true;
            chanelex = chanelex.And(t => !t.IsDelete);
            chanelex = chanelex.And(t => t.UserId == userId);
            var UserChanelList = _userChanelRepository.Where(chanelex).ToList();
            if (UserChanelList != null && UserChanelList.Count > 0)
            {
                chanelIds = UserChanelList.Select(zw => zw.ChannelId).ToList();
            }
            return chanelIds;
        }
        public IQueryable<UserChanel> Entities()
        {
            return _userChanelRepository.AsQueryable();
        }
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="userChanelIds"></param>
        /// <returns></returns>
        public async Task BlukDeleteAsync(List<int> userChanelIds)
        {
            try
            {
                await _userChanelRepository.DeleteAsync(zw => userChanelIds.Contains(zw.Id), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="userChanels"></param>
        /// <returns></returns>
        public async Task BlukInsertAsync(List<UserChanel> userChanels)
        {
            try
            {
                foreach (var item in userChanels)
                {
                    if (item.ChannelId > 0)
                    {
                        await _userChanelRepository.InsertAsync(item, true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
