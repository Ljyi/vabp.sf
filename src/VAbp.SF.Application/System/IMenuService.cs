﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.System.Query;

namespace VAbp.SF.System
{
    public interface IMenuService
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="addUserDto">添加</param>
        /// <returns></returns>
        Task<bool> AddMenu(AddMenuDto addMenuDto);
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="editUserDto"></param>
        /// <returns></returns>
        Task<bool> EditMenu(EditMenuDto editUserDto);
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<MenuInfo> GetMenuById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Menu> FindMenuById(int id);
        /// <summary>
        /// 查询全部
        /// </summary>
        /// <returns></returns>
        List<MenuInfo> GetMenuAll();
        /// <summary>
        /// 查询全部
        /// </summary>
        /// <returns></returns>
        List<MenuInfo> GetMenuGrid(MenuQuery menuQuery);
        /// <summary>
        /// 自动生成编码
        /// </summary>
        /// <returns></returns>
        Task<string> GetNextMenuCode(string menuCode);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IQueryable<Menu> Entities();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        Task<bool> DeleteAsync(int id, string userName);
    }
}