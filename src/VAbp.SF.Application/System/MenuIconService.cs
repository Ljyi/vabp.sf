﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.System.Query;
using VAbp.SF.System.Repository;

namespace VAbp.SF.System
{
    public class MenuIconService : ServiceBase, IMenuIconService
    {
        private readonly IMenuIconRepository _menuIconRepository;
        public MenuIconService(IMenuIconRepository menuIconRepository)
        {
            _menuIconRepository = menuIconRepository;
        }
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<MenuIconInfo> GetMenuIconById(int id)
        {
            var menuIconInfo = await _menuIconRepository.FindAsync(id);
            return ObjectMapper.Map<System.MenuIcon, MenuIconInfo>(menuIconInfo);
        }
        /// <summary>
        /// 查询全部
        /// </summary>
        /// <returns></returns>
        public List<MenuIconDto> GetMenuIconAll()
        {
            var menuIconInfos = _menuIconRepository.WhereIf(true, zw => !zw.IsDelete && zw.Status == 1);
            return ObjectMapper.Map<IEnumerable<System.MenuIcon>, List<MenuIconDto>>(menuIconInfos);
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="menuIconQuery"></param>
        /// <returns></returns>
        public List<MenuIconInfo> GetMenuIconGrid(MenuIconQuery menuIconQuery)
        {
            var result = new ServiceResult<PagedList<MenuIconInfo>>();
            var query = _menuIconRepository.WhereIf(menuIconQuery.Code.IsNotNullOrEmpty(), x => x.Code == menuIconQuery.Code);
            menuIconQuery.TotalCount = query.Count();
            var where = query.PageByIndex(menuIconQuery.PageIndex, menuIconQuery.PageSize);
            var list = ObjectMapper.Map<IEnumerable<System.MenuIcon>, List<MenuIconInfo>>(where);
            return list;
        }
    }
}
