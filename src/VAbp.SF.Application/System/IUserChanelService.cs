﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VAbp.SF.System
{
    public interface IUserChanelService
    {
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        List<int> GetUserChanelByUserId(int userId);

        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        IQueryable<UserChanel> Entities();
        Task BlukDeleteAsync(List<int> userChanelIds);
        Task BlukInsertAsync(List<UserChanel> userChanels);
    }
}
