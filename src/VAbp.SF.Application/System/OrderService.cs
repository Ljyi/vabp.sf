﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base.Extensions;
using VAbp.SF.System.Query;
using VAbp.SF.System.Repository;

namespace VAbp.SF.System
{
    public class OrderService : ServiceBase, IOrderService
    {
        private IOrderRepository _orderRepository;
        IChannelService _channelService;
        private readonly IUserChanelRepository _userChanelRepository;
        private static readonly object locker = new object();
        public OrderService(IOrderRepository orderRepository, IChannelService channelService, IUserChanelRepository userChanelRepository)
        {
            this._orderRepository = orderRepository;
            this._channelService = channelService;
            _userChanelRepository = userChanelRepository;
        }

        public async Task<OrderInfo> GetOrderById(int id)
        {
            try
            {
                Order order = await _orderRepository.FindAsync(id);
                return ObjectMapper.Map<Order, OrderInfo>(order);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OrderInfo> GetOrderAll()
        {
            try
            {
                var orderList = _orderRepository.WhereIf(true, zw => !zw.IsDelete);
                return ObjectMapper.Map<IEnumerable<Order>, List<OrderInfo>>(orderList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="orderQuery"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<OrderDto> GetOrderGrid(OrderQuery orderQuery, int type = 0, bool IsAdmin = false)
        {
            try
            {
                string where = " and IsDelete=0";
                StringBuilder stringBuilder = new StringBuilder();
                Expression<Func<Order, bool>> orderex = t => true;
                orderex = orderex.And(t => !t.IsDelete);
                if (!string.IsNullOrEmpty(orderQuery.Phone))
                {
                    where += "  and Phone like '%" + orderQuery.Phone + "%'";
                    orderex = orderex.And(t => t.Phone.Contains(orderQuery.Phone));
                }
                if (!string.IsNullOrEmpty(orderQuery.UserName))
                {
                    where += "  and UserName like '%" + orderQuery.UserName + "%'";
                    orderex = orderex.And(t => t.UserName.Contains(orderQuery.UserName));
                }
                if (orderQuery.Channel.Count > 0)
                {
                    // 获取通道
                    orderex = orderex.And(x => orderQuery.Channel.Contains(x.Channel));
                }
                if (!IsAdmin)
                {
                    orderex = orderex.And(t => !t.Address.Contains("地址待确认"));
                    where += "  and Address not like '%地址待确认%'";
                }
                if (!string.IsNullOrEmpty(orderQuery.StartDatetime) && !string.IsNullOrEmpty(orderQuery.EndDatetime))
                {
                    DateTime startDatetime = Convert.ToDateTime(orderQuery.StartDatetime);
                    DateTime endDatetime = Convert.ToDateTime(orderQuery.EndDatetime);
                    orderex = orderex.And(t => t.CredateTime >= startDatetime && t.CredateTime <= endDatetime);
                    where += "  and CredateTime>='" + startDatetime + "' and CredateTime<='" + endDatetime + "'";
                }
                var query = _orderRepository.Where(orderex).OrderByDescending(zw => zw.Id);
                if (!IsAdmin)
                {
                    int userId = orderQuery.UserId;
                    var userChanels = _userChanelRepository.Where(zw => !zw.IsDelete && zw.UserId == userId && orderQuery.Channel.Contains(zw.ChannelId));
                    foreach (var item in userChanels)
                    {

                        string sql = "SELECT * FROM [Order] WHERE  Channel=" + item.ChannelId + where;
                        if (item.StartTime.HasValue && item.EndTime.HasValue)
                        {
                            sql = "SELECT * FROM [Order] WHERE Channel=" + item.ChannelId + " AND CredateTime >'" + item.StartTime.Value + "' AND CredateTime<'" + item.EndTime.Value + "'" + where;
                        }
                        if (stringBuilder.Length < 1)
                        {
                            stringBuilder.Append(sql);
                        }
                        else
                        {
                            sql = " UNION ALL " + sql;
                            stringBuilder.Append(sql);
                        }
                    }
                }
                else
                {
                    // 获取通道
                    if (orderQuery.Channel.Count > 0)
                    {
                        where += "  and Channel in(" + orderQuery.Channel.ToSeparateString() + ")";
                    }
                    string sql = "SELECT * FROM [Order] WHERE  1=1 " + where;
                    stringBuilder.Append(sql);
                }
                var sqlStringBuilder = stringBuilder.ToString();
                var query2 = _orderRepository.GetOrders(sqlStringBuilder);
                query2 = query2.OrderByDescending(zw => zw.Id);
                List<Order> orders = new List<Order>();
                if (type == 0)
                {
                    orders = query2.PageByIndex(orderQuery.PageIndex, orderQuery.PageSize).ToList();
                }
                else
                {
                    orders = query2.ToList();
                }
                var list = orders.Select(zw => new OrderDto()
                {
                    Address = zw.Address,
                    Channel = zw.Channel,
                    CreateUser = zw.CreateUser,
                    CredateTime = zw.CredateTime,
                    Id = zw.Id,
                    OrderNo = zw.OrderNo,
                    Phone = zw.Phone,
                    Remark = zw.Remark,
                    UserName = zw.UserName,
                    UpdateTime = zw.UpdateTime,
                    UpdateUser = zw.UpdateUser

                }).ToList();
                orderQuery.TotalCount = query2.Count();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> DeleteAsync(int id, string userName)
        {
            var order = await _orderRepository.FindAsync(id);
            order.IsDelete = true;
            order.UpdateTime = DateTime.Now;
            order.UpdateUser = userName;
            await _orderRepository.UpdateAsync(order);
            return true;
        }
        /// <summary>
        /// 创建订单
        /// </summary>
        /// <param name="orderDto"></param>
        /// <returns></returns>
        public async Task<bool> Add(AddOrderDto orderDto)
        {
            Expression<Func<Order, bool>> orderex = t => true;
            orderex = orderex.And(t => !t.IsDelete);
            orderex = orderex.And(t => t.Phone == orderDto.Phone);
            var orderQuery = _orderRepository.FirstOrDefault(orderex);
            if (orderQuery == null)
            {
                Order order = new Order()
                {
                    OrderNo = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    IsDelete = false,
                    Address = orderDto.Address,
                    Channel = orderDto.Channel,
                    CreateUser = "API",
                    CredateTime = DateTime.Now,
                    Phone = orderDto.Phone,
                    Remark = orderDto.Remark,
                    UserName = orderDto.UserName
                };
                await _orderRepository.InsertAsync(order, true);
            }
            return true;
        }
        /// <summary>
        /// 订单统计
        /// </summary>
        /// <param name="channelIds"></param>
        /// <returns></returns>
        public List<OrderCount> GetOrderCounts(List<int> channelIds)
        {
            List<OrderCount> orderCounts = new List<OrderCount>();
            Expression<Func<Order, bool>> orderex = t => true;
            orderex = orderex.And(t => !t.IsDelete);
            if (channelIds.Count > 0)
            {
                orderex = orderex.And(x => channelIds.Contains(x.Channel));
            }
            var query = _orderRepository.Where(orderex);
            OrderCount orderAllCount = new OrderCount()
            {
                Name = "All",
                Count = query.Count()
            };
            DateTime startDatetime = DateTime.Parse(DateTime.Now.Date.ToString() + " 00:00:00");
            DateTime endDatetime = DateTime.Parse(DateTime.Now.Date.ToString() + " 23:59:59");
            OrderCount orderDateCount = new OrderCount()
            {
                Name = "Today",
                Count = query.Where(t => t.CredateTime >= startDatetime && t.CredateTime <= endDatetime).Count()
            };
            return orderCounts;
        }
        /// <summary>
        /// 渠道统计
        /// </summary>
        /// <param name="channelIds"></param>
        /// <returns></returns>
        public List<OrderChannel> GetOrderChannel(List<int> channelIds, int userId)
        {
            List<OrderChannel> orderChannels = new List<OrderChannel>();
            Expression<Func<Order, bool>> orderex = t => true;
            orderex = orderex.And(t => !t.IsDelete);
            if (channelIds.Count > 0)
            {
                orderex = orderex.And(x => channelIds.Contains(x.Channel));
            }
            var query = _orderRepository.Where(orderex);
            var channelList = _channelService.GetChannelAll();
            if (userId != 1)
            {
                StringBuilder stringBuilder = new StringBuilder();
                var userChanels = _userChanelRepository.Where(zw => !zw.IsDelete && zw.UserId == userId && channelIds.Contains(zw.ChannelId));
                foreach (var item in userChanels)
                {
                    string sql = "SELECT * FROM [Order] WHERE IsDelete=0 AND Channel=" + item.ChannelId;
                    if (item.StartTime.HasValue && item.EndTime.HasValue)
                    {
                        sql = "SELECT * FROM [Order] WHERE IsDelete=0 AND Channel=" + item.ChannelId + " AND CredateTime >'" + item.StartTime.Value + "' AND CredateTime<'" + item.EndTime.Value + "'";
                    }
                    if (stringBuilder.Length < 1)
                    {
                        stringBuilder.Append(sql);
                    }
                    else
                    {
                        sql = " UNION ALL " + sql;
                        stringBuilder.Append(sql);
                    }
                }
                var sqlStringBuilder = stringBuilder.ToString();
                query = _orderRepository.GetOrders(sqlStringBuilder);
            }
            var group = query.ToList().GroupBy(zw => zw.Channel);
            OrderChannel orderChannelAll = new OrderChannel()
            {
                Id = 0,
                Count = query.Count(),
                Name = "全部"
            };
            orderChannels.Add(orderChannelAll);
            foreach (var item in group)
            {
                OrderChannel orderChannel = new OrderChannel()
                {
                    Id = item.Key,
                    Count = item.Count(),
                    Name = channelList.FirstOrDefault(zw => zw.Id == item.Key).ChannelName
                };
                orderChannels.Add(orderChannel);
            }
            return orderChannels;
        }

    }
}
