﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VAbp.SF.System
{
    public interface IUserPermissionService
    {
        /// <summary>
        /// 根据用户Id查询菜单
        /// </summary>
        /// <returns></returns>
        List<MenuInfo> GetUserMenus(int userId, bool isAdmin = false);
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="userPermissionsIds"></param>
        Task BlukDeleteAsync(List<int> userPermissionsIds);
        /// <summary>
        /// 批量插入
        /// </summary>
        /// <param name="userPermissions"></param>
        Task BlukInsertAsync(List<UserPermission> userPermissions);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IQueryable<UserPermission> Entities();
    }
}
