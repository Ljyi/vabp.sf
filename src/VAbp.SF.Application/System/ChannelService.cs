﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base.Extensions;
using VAbp.SF.System.Query;
using VAbp.SF.System.Repository;

namespace VAbp.SF.System
{
    public class ChannelService : ServiceBase, IChannelService
    {
        private IChannelRepository _channelRepository;
        IUserChanelService userChanelService;
        private static readonly object locker = new object();
        public ChannelService(IChannelRepository channelRepository, IUserChanelService userChanelService)
        {
            this._channelRepository = channelRepository;
            this.userChanelService = userChanelService;
        }

        public async Task<ChannelDto> GetChannelById(int id)
        {
            try
            {
                Channel channel = await _channelRepository.FindAsync(id);
                ChannelDto channelInfo = new ChannelDto();
                channelInfo.Id = channel.Id;
                channelInfo.ChannelCode = channel.ChannelCode;
                channelInfo.ChannelName = channel.ChannelName;
                channelInfo.Sort = channel.Sort;
                channelInfo.Enable = channel.Enable;
                channelInfo.CreateUser = channel.CreateUser;
                channelInfo.CredateTime = channel.CreateTime;
                channelInfo.UpdateUser = channel.UpdateUser;
                channelInfo.UpdateTime = channel.UpdateTime;
                if (channel.ParentId != 0)
                {
                    channelInfo.ParentId = new List<int>() { 0, channel.ParentId, channel.Id };
                }
                return channelInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ChannelInfo> GetChannelAll()
        {
            try
            {
                var channelList = _channelRepository.WhereIf(true, zw => !zw.IsDelete);
                return ObjectMapper.Map<IEnumerable<Channel>, List<ChannelInfo>>(channelList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ChannelTree> GetChannelTree()
        {
            try
            {
                List<ChannelTree> channelTrees = new List<ChannelTree>();
                var channelList = _channelRepository.WhereIf(true, zw => !zw.IsDelete && zw.Enable).ToList();
                if (channelList.Count > 0)
                {
                    var channelParentList = channelList.Where(zw => zw.ParentId == 0);
                    foreach (var item in channelParentList)
                    {
                        ChannelTree channelTree = new ChannelTree();
                        channelTree.Id = item.Id;
                        channelTree.ChannelName = item.ChannelName;
                        channelTree.Sort = item.Sort;
                        List<ChannelTree> list = new List<ChannelTree>();
                        var childChannelList = channelList.Where(zw => zw.ParentId == item.Id);
                        foreach (var itemChild in childChannelList)
                        {
                            if (itemChild.ParentId != 0)
                            {
                                ChannelTree channelChildTree = new ChannelTree();
                                channelChildTree.Id = itemChild.Id;
                                channelChildTree.ChannelName = itemChild.ChannelName;
                                channelChildTree.Sort = itemChild.Sort;
                                list.Add(channelChildTree);
                            }
                        }
                        if (list.Count > 0)
                        {
                            channelTree.ChannelChildTrees = list.OrderBy(zw => zw.Sort).ToList();
                        }
                        channelTrees.Add(channelTree);
                    }
                }
                if (channelTrees.Count > 0)
                {
                    channelTrees = channelTrees.OrderBy(zw => zw.Sort).ToList();
                }
                return channelTrees;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ChannelTree GetSelectChannelTree()
        {
            try
            {
                ChannelTree channel = new ChannelTree()
                {
                    Id = 0,
                    ChannelName = "--顶级--",
                    Sort = 1
                };
                var channelList = _channelRepository.WhereIf(true, zw => !zw.IsDelete && zw.Enable).ToList();
                if (channelList.Count > 0)
                {
                    var channelParentList = channelList.Where(zw => zw.ParentId == 0);
                    foreach (var item in channelParentList)
                    {
                        ChannelTree channelTree = new ChannelTree();
                        channelTree.Id = item.Id;
                        channelTree.ChannelName = item.ChannelName;
                        channelTree.Sort = item.Sort;
                        List<ChannelTree> list = new List<ChannelTree>();
                        var childChannelList = channelList.Where(zw => zw.ParentId == item.Id);
                        foreach (var itemChild in childChannelList)
                        {
                            if (itemChild.ParentId != 0)
                            {
                                ChannelTree channelChildTree = new ChannelTree();
                                channelChildTree.Id = itemChild.Id;
                                channelChildTree.ChannelName = itemChild.ChannelName;
                                channelChildTree.Sort = itemChild.Sort;
                                list.Add(channelChildTree);
                            }
                        }
                        if (list.Count > 0)
                        {
                            channelTree.ChannelChildTrees = list.OrderBy(zw => zw.Sort).ToList();
                        }

                        channel.ChannelChildTrees.Add(channelTree);
                    }
                }
                return channel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ChannelTree> GetUserChannelTree(int userId)
        {
            try
            {
                var channelIds = userChanelService.Entities().Where(zw => !zw.IsDelete && zw.UserId == userId).ToList().Select(zw => zw.ChannelId).ToList();
                List<ChannelTree> channelTrees = new List<ChannelTree>();
                var channelList = _channelRepository.WhereIf(true, zw => !zw.IsDelete && zw.Enable && channelIds.Contains(zw.Id)).ToList();
                var channelAllList = _channelRepository.WhereIf(true, zw => !zw.IsDelete).ToList();
                foreach (var item in channelList)
                {
                    //父级节点
                    ChannelTree channelTree = new ChannelTree();
                    int parentId = 0;
                    if (item.ParentId != 0)
                    {
                        parentId = item.ParentId;
                        var parentChanne = channelAllList.FirstOrDefault(zw => zw.Id == item.ParentId);
                        channelTree.Id = parentChanne.Id;
                        channelTree.ChannelName = parentChanne.ChannelName;
                        channelTree.Sort = parentChanne.Sort;
                    }
                    else
                    {
                        parentId = item.Id;
                        channelTree.Id = item.Id;
                        channelTree.ChannelName = item.ChannelName;
                        channelTree.Sort = item.Sort;
                    }
                    //子集节点
                    List<ChannelTree> list = new List<ChannelTree>();
                    var childChannel = channelAllList.Where(zw => zw.ParentId == parentId && channelIds.Contains(zw.Id)).ToList();
                    foreach (var itemChild in childChannel)
                    {
                        ChannelTree channelChildTree = new ChannelTree();
                        channelChildTree.Id = itemChild.Id;
                        channelChildTree.ChannelName = itemChild.ChannelName;
                        channelChildTree.Sort = itemChild.Sort;
                        list.Add(channelChildTree);
                    }
                    if (list.Count > 0)
                    {
                        channelTree.ChannelChildTrees = list.OrderBy(zw => zw.Sort).ToList();
                    }
                    channelTrees.Add(channelTree);
                }
                return channelTrees.OrderBy(zw => zw.Sort).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ChannelInfo> GetChannelGrid(ChannelQuery channelQuery)
        {
            try
            {
                Expression<Func<Channel, bool>> channelex = t => true;
                channelex = channelex.And(t => !t.IsDelete);
                if (!string.IsNullOrEmpty(channelQuery.Name))
                {
                    channelex = channelex.And(t => t.ChannelName.Contains(channelQuery.Name));
                }
                //if (channelQuery.ParentId.HasValue)
                //{
                //    channelex = channelex.And(x => x.ParentId == channelQuery.ParentId.Value);
                //}
                var query = _channelRepository.Where(channelex);
                var where = query.PageByIndex(channelQuery.PageIndex, channelQuery.PageSize);
                var list = ObjectMapper.Map<IEnumerable<Channel>, List<ChannelInfo>>(where);
                channelQuery.TotalCount = query.Count();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="addChannelDto"></param>
        /// <returns></returns>
        public async Task<bool> AddChannel(AddChannelDto addChannelDto)
        {
            try
            {
                //主单
                Channel channel = ObjectMapper.Map<AddChannelDto, Channel>(addChannelDto);
                await _channelRepository.InsertAsync(channel, true);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> EditChannel(EditChannelDto editChannelDto)
        {
            try
            {
                Channel channel = await _channelRepository.FindAsync(editChannelDto.Id);
                channel.ChannelCode = editChannelDto.ChannelCode;
                channel.ChannelName = editChannelDto.ChannelName;
                channel.ParentId = editChannelDto.ParentId;
                channel.Sort = editChannelDto.Sort;
                channel.Enable = editChannelDto.Enable;
                channel.UpdateUser = editChannelDto.UpdateUser;
                channel.UpdateTime = editChannelDto.UpdateTime;
                await _channelRepository.UpdateAsync(channel);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IQueryable<Channel> Entities()
        {
            return _channelRepository.AsQueryable();
        }

        public async Task<bool> DeleteAsync(int id, string userName)
        {
            Channel channel = await _channelRepository.FindAsync(id);
            channel.IsDelete = true;
            channel.UpdateTime = DateTime.Now;
            channel.UpdateUser = userName;
            await _channelRepository.UpdateAsync(channel);
            return true;
        }
    }
}
