﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.System.Query;
using VAbp.SF.System.Repository;

namespace VAbp.SF.System
{
    public class MenuService : ServiceBase, IMenuService
    {
        private IMenuRepository _menuRepository;
        IPermissionRepository permissionRepository;
        private static readonly object locker = new object();
        public MenuService(IMenuRepository menuRepository, IPermissionRepository permissionRepository)
        {
            this._menuRepository = menuRepository;
            this.permissionRepository = permissionRepository;
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="addMenuDto"></param>
        /// <returns></returns>
        public async Task<bool> AddMenu(AddMenuDto addMenuDto)
        {
            try
            {
                //主单
                Menu menu = ObjectMapper.Map<AddMenuDto, Menu>(addMenuDto);
                menu.MenuCode = await GetNextMenuCode("");
                menu = await _menuRepository.InsertAsync(menu,true);
                Permission permission = new Permission()
                {
                    MenuId = menu.Id,
                    MenuAlias = "",
                    Name = menu.MenuName,
                    ActionCode = "view",
                    Status = 1,
                    Type = 0,
                    CreateTime = DateTime.Now,
                    CreateUser = "system"
                };
                permission.Code = await GetNextPermissionCode("");
                await permissionRepository.InsertAsync(permission, true);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="editUserDto"></param>
        /// <returns></returns>
        public async Task<bool> EditMenu(EditMenuDto editMenuDto)
        {
            try
            {
                Menu menu = await _menuRepository.FindAsync(editMenuDto.Id);
                menu.MenuName = editMenuDto.MenuName;
                menu.Url = editMenuDto.Url;
                menu.MenuLevel = editMenuDto.MenuLevel;
                menu.ParentId = editMenuDto.ParentId;
                menu.Sort = editMenuDto.Sort;
                menu.Icon = editMenuDto.Icon;
                menu.Description = editMenuDto.Description;
                menu.Component = editMenuDto.Component;
                menu.Alias = editMenuDto.Alias;
                menu.UpdateUser = editMenuDto.UpdateUser;
                menu.UpdateTime = editMenuDto.UpdateTime;
                menu.HideInMenu = editMenuDto.HideInMenu;
                menu.IsShow = editMenuDto.IsShow;
                menu.NotCache = editMenuDto.NotCache;
                await _menuRepository.UpdateAsync(menu);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 根据Id获取
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<MenuInfo> GetMenuById(int id)
        {
            try
            {
                Menu menu = await _menuRepository.FindAsync(id);
                return ObjectMapper.Map<Menu, MenuInfo>(menu);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 根据Id获取
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Menu> FindMenuById(int id)
        {
            try
            {
                return await _menuRepository.FindAsync(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 获取全部
        /// </summary>
        /// <returns></returns>
        public List<MenuInfo> GetMenuAll()
        {
            try
            {
                var menuList = _menuRepository.WhereIf(true, zw => !zw.IsDelete);
                return ObjectMapper.Map<IEnumerable<System.Menu>, List<MenuInfo>>(menuList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 分页获取
        /// </summary>
        /// <param name="pageParameter"></param>
        /// <returns></returns>
        public List<MenuInfo> GetMenuGrid(MenuQuery menuQuery)
        {
            try
            {
                Expression<Func<Menu, bool>> menuex = t => true;
                menuex = menuex.And(t => !t.IsDelete);
                if (!string.IsNullOrEmpty(menuQuery.Kw))
                {
                    menuex = menuex.And(t => t.MenuName.Contains(menuQuery.Kw));
                }
                if (menuQuery.ParentId.HasValue)
                {
                    menuex = menuex.And(x => x.ParentId == menuQuery.ParentId.Value);
                }
                var query = _menuRepository.Where(menuex);
                var where = query.PageByIndex(menuQuery.PageIndex, menuQuery.PageSize);
                var list = ObjectMapper.Map<IEnumerable<System.Menu>, List<MenuInfo>>(where);
                menuQuery.TotalCount = query.Count();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetNextMenuCode(string menuCode)
        {
            try
            {
                if (!string.IsNullOrEmpty(menuCode) && menuCode.Length == 4)
                {
                    lock (locker)
                    {
                        string code = menuCode;
                        if (_menuRepository.WhereIf(true, p => p.MenuCode != code).IsIn())
                        {
                            var menu = _menuRepository.GetListAsync().Result.OrderByDescending(p => p.MenuCode).FirstOrDefault();
                            int codeNum = int.Parse(menu.MenuCode) + 1;
                            menuCode = Math.Round((codeNum / 1000m), 3).ToString("f3").Replace(".", "");
                        }
                    }
                    return menuCode;
                }
                else
                {
                    menuCode = "0001";
                    return await GetNextMenuCode(menuCode);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                throw;
            }
        }
        public async Task<string> GetNextPermissionCode(string permissionCode)
        {
            try
            {
                if (!string.IsNullOrEmpty(permissionCode) && permissionCode.Length == 5)
                {
                    lock (locker)
                    {
                        string code = permissionCode;
                        if (_menuRepository.WhereIf(true, p => p.MenuCode != code).IsIn())
                        {
                            var permission = permissionRepository.GetListAsync().Result.OrderByDescending(p => p.Code).FirstOrDefault();
                            int codeNum = int.Parse(permission.Code) + 1;
                            permissionCode = Math.Round((codeNum / 10000m), 4).ToString("f4").Replace(".", "");
                        }
                    }
                    return permissionCode;
                }
                else
                {
                    permissionCode = "00001";
                    return await GetNextPermissionCode(permissionCode);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                throw;
            }
        }

        public IQueryable<Menu> Entities()
        {
            return _menuRepository.AsQueryable();
        }

        public async Task<bool> DeleteAsync(int id, string userName)
        {
            var menu = await _menuRepository.FindAsync(id);
            menu.IsDelete = true;
            menu.UpdateTime = DateTime.Now;
            menu.UpdateUser = userName;
            await _menuRepository.UpdateAsync(menu);
            return true;
        }

        ///// <summary>
        ///// 根据用户Id查询菜单
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <param name="isAdmin"></param>
        ///// <returns></returns>
        //public async Task<List<MenuInfo>> GetUserMenus(int userId, bool isAdmin = false)
        //{
        //    List<MenuInfo> menuInfos = new List<MenuInfo>();
        //    List<Menu> menus = new List<Menu>();
        //    if (isAdmin)
        //    {
        //        menus =  _menuRepository.GetListAsync(zw => !zw.IsDelete).ToList();
        //    }
        //    else
        //    {
        //        var userPermissions = _userPermissionRepository.WhereIf(true, zw => !zw.IsDelete && zw.UserId == userId).ToList();
        //        List<int> menuIds = userPermissions.Select(zw => zw.Permission.MenuId).Distinct().ToList();
        //        menus = await menuService.GetAllListAsync(zw => !zw.IsDelete && menuIds.Contains(zw.Id));
        //    }
        //    menuInfos = _mapper.Map<List<MenuInfo>>(menus);
        //    return menuInfos;
        //}
    }
}
