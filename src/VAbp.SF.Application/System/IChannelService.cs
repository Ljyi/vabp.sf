﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.System.Query;

namespace VAbp.SF.System
{
    public interface IChannelService
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="addUserDto">添加</param>
        /// <returns></returns>
        Task<bool> AddChannel(AddChannelDto addChannelDto);
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="editUserDto"></param>
        /// <returns></returns>
        Task<bool> EditChannel(EditChannelDto editChannelDto);
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ChannelDto> GetChannelById(int id);
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        List<ChannelInfo> GetChannelAll();
        /// <summary>
        /// 获取渠道
        /// </summary>
        /// <returns></returns>
        List<ChannelTree> GetChannelTree();

        ChannelTree GetSelectChannelTree();
        /// <summary>
        /// 获取渠道
        /// </summary>
        /// <returns></returns>
        List<ChannelTree> GetUserChannelTree(int userId);
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <returns></returns>
        List<ChannelInfo> GetChannelGrid(ChannelQuery channelQuery);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IQueryable<Channel> Entities();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        Task<bool> DeleteAsync(int id, string userName);
    }
}
