﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.System.Query;

namespace VAbp.SF.System
{
    public interface IMenuIconService
    {
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<MenuIconInfo> GetMenuIconById(int id);
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        List<MenuIconDto> GetMenuIconAll();
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <returns></returns>
        List<MenuIconInfo> GetMenuIconGrid(MenuIconQuery menuIconQuery);
    }
}
