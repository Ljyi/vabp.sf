﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.System.Query;
using VAbp.SF.System.Repository;
using Volo.Abp.ObjectMapping;

namespace VAbp.SF.System
{
    public class UserService : ServiceBase, IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<bool> AddUser(AddUserDto addUserDto)
        {
            try
            {
                addUserDto.Password = EncryptHelper.GetMD5_32(addUserDto.Password);
                var user = ObjectMapper.Map<AddUserDto, VAbp.SF.System.User>(addUserDto);
                await _userRepository.InsertAsync(user);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteByUserIdAsync(int id)
        {
            bool success = true;
            try
            {
                var user = await _userRepository.FindAsync(id);
                user.IsDelete = true;
                await _userRepository.UpdateAsync(user, true);

            }
            catch (Exception ex)
            {
                success = false;
            }
            return success;
        }

        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <param name="editUserDto"></param>
        /// <returns></returns>
        public async Task<bool> EditUser(ModifyUserDto editUserDto)
        {
            try
            {
                User user = await _userRepository.FindAsync(editUserDto.Id);
                if (editUserDto != null)
                {
                    user.UserName = editUserDto.UserName;
                    user.HeadImg = editUserDto.HeadImg;
                    user.Email = editUserDto.Email;
                    user.UpdateUser = editUserDto.UpdateUser;
                    user.UpdateTime = editUserDto.UpdateTime;
                }
                await _userRepository.UpdateAsync(user);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IQueryable<User> Entities()
        {

            return _userRepository.AsQueryable();
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<UserInfo> GetUserById(int id)
        {
            User user = await _userRepository.FindAsync(id);
            var userInfo = ObjectMapper.Map<VAbp.SF.System.User, UserInfo>(user);
            return userInfo;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userQuery"></param>
        /// <returns></returns>
        public List<UserInfo> GetUserGrid(UserQuery userQuery)
        {
            Expression<Func<User, bool>> appuerex = t => true;
            appuerex = appuerex.And(t => !t.IsDelete);
            if (!string.IsNullOrEmpty(userQuery.Keywords))
            {
                appuerex = appuerex.And(t => t.UserName.Contains(userQuery.Keywords));
            }
            var query = _userRepository.Where(appuerex).OrderByDescending(x => x.CredateTime);
            var where = query.PageByIndex(userQuery.PageIndex, userQuery.PageSize);
            var list = ObjectMapper.Map<IEnumerable<System.User>, List<UserInfo>>(where);
            userQuery.TotalCount = query.Count();
            return list;

            //var result = new ServiceResult<PagedList<UserDto>>();
            //Expression<Func<User, bool>> appuerex = t => true;
            //appuerex = appuerex.And(t => !t.IsDelete);
            //if (!string.IsNullOrEmpty(userQuery.Keywords))
            //{
            //    appuerex = appuerex.And(t => t.UserName.Contains(userQuery.Keywords));
            //}
            //var query = _userRepository.Where(appuerex).OrderByDescending(x => x.CredateTime);
            //var count = query.Count();
            //var where = query.PageByIndex(userQuery.Index, userQuery.Limit);
            //List<UserDto> list = new List<UserDto>();
            //try
            //{
            //    list = ObjectMapper.Map<IEnumerable<System.User>, List<UserDto>>(where);
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            //result.IsSuccess(new PagedList<UserDto>(count, list));
            //return await Task.FromResult(result);
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="account">账号</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        public async Task<UserInfo> Login(string account, string password)
        {
            try
            {
                Expression<Func<User, bool>> appuerex = t => true;
                appuerex = appuerex.And(t => !t.IsDelete);
                appuerex = appuerex.And(t => t.Account == account);
                appuerex = appuerex.And(t => t.Password.ToLower() == password.ToLower());
                var appuser = await _userRepository.FindAsync(appuerex);
                if (appuser != null)
                {
                    return ObjectMapper.Map<User, UserInfo>(appuser);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <param name="updateUser"></param>
        /// <returns></returns>
        public async Task<bool> UpdateUser(UpdateUserDto updateUser)
        {
            try
            {
                User user = await _userRepository.FindAsync(updateUser.Id);
                if (updateUser != null)
                {
                    user.UserName = updateUser.UserName;
                    user.UpdateUser = updateUser.Email;
                    user.Password = updateUser.Password;
                    user.UpdateUser = updateUser.UpdateUser;
                    user.UpdateTime = updateUser.UpdateTime;
                }
                await _userRepository.UpdateAsync(user);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
