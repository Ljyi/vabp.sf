﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.System.Query;

namespace VAbp.SF.System
{
    public interface IOrderService
    {
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> Add(AddOrderDto orderDto);
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<OrderInfo> GetOrderById(int id);
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        List<OrderInfo> GetOrderAll();
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <returns></returns>
        List<OrderDto> GetOrderGrid(OrderQuery orderQuery, int type = 0, bool isAdmin = false);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        Task<bool> DeleteAsync(int id, string userName);
        /// <summary>
        /// 订单查询
        /// </summary>
        /// <returns></returns>
        List<OrderCount> GetOrderCounts(List<int> channelIds);
        /// <summary>
        /// 渠道订单
        /// </summary>
        /// <param name="channelIds"></param>
        /// <returns></returns>
        List<OrderChannel> GetOrderChannel(List<int> channelIds, int userId);
    }
}
