﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.System.Query;

namespace VAbp.SF.System
{
    public interface IPermissionService
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="addUserDto">添加</param>
        /// <returns></returns>
        Task<bool> AddPermission(AddPermissionDto addPermission);
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="editUserDto"></param>
        /// <returns></returns>
        Task<bool> EditPermission(EditPermissionDto editPermission);
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<PermissionInfo> GetPermissionById(int id);
        /// <summary>
        /// 根据菜单Id查询
        /// </summary>
        /// <returns></returns>
        List<PermissionInfo> GetPermissionByMenuId(int menuId);
        /// <summary>
        /// 查询全部
        /// </summary>
        /// <returns></returns>
        List<PermissionInfo> GetPermissionList();
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <returns></returns>
        List<PermissionInfo> GetPermissionGrid(PermissionQuery permissionQuery);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IQueryable<Permission> Entities();
    }
}