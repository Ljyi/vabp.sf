﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.Base.Extensions;
using VAbp.SF.System.Query;
using VAbp.SF.System.Repository;

namespace VAbp.SF.System
{
    public class PermissionService : ServiceBase, IPermissionService
    {
        private IPermissionRepository _permissionRepository;
        private static readonly object locker = new object();
        public PermissionService(IPermissionRepository permissionRepository)
        {
            this._permissionRepository = permissionRepository;
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="addPermission"></param>
        /// <returns></returns>
        public async Task<bool> AddPermission(AddPermissionDto addPermission)
        {
            try
            {
                //主单
                Permission permission = ObjectMapper.Map<AddPermissionDto, Permission>(addPermission);
                permission.Code = await GetNextPermissionCode("");
                await _permissionRepository.InsertAsync(permission);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="editPermission"></param>
        /// <returns></returns>
        public async Task<bool> EditPermission(EditPermissionDto editPermission)
        {
            try
            {
                //主单
                Permission permission = await _permissionRepository.FindAsync(editPermission.Id);
                permission.MenuId = editPermission.MenuId;
                permission.Name = editPermission.Name;
                permission.Icon = editPermission.Icon;
                permission.ActionCode = editPermission.ActionCode;
                permission.Description = editPermission.Description;
                permission.Status = editPermission.Status;
                permission.Type = editPermission.Type;
                permission.MenuAlias = editPermission.MenuAlias;
                permission.UpdateTime = editPermission.UpdateTime;
                permission.UpdateUser = editPermission.UpdateUser;
                await _permissionRepository.UpdateAsync(permission);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<PermissionInfo> GetPermissionById(int id)
        {
            Permission permission = await _permissionRepository.FindAsync(id);
            return ObjectMapper.Map<Permission, PermissionInfo>(permission);
        }
        /// <summary>
        /// 根据菜单Id
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public List<PermissionInfo> GetPermissionByMenuId(int menuId)
        {
            List<Permission> permissions = _permissionRepository.WhereIf(true, zw => zw.MenuId == menuId && !zw.IsDelete).ToList();
            return ObjectMapper.Map<List<Permission>, List<PermissionInfo>>(permissions);
        }
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="pageParameter"></param>
        /// <returns></returns>
        public List<PermissionInfo> GetPermissionGrid(PermissionQuery permissionQuery)
        {
            try
            {
                Expression<Func<Permission, bool>> menuex = t => true;
                menuex = menuex.And(t => !t.IsDelete);
                if (!string.IsNullOrEmpty(permissionQuery.key))
                {
                    menuex = menuex.And(t => t.Name.Contains(permissionQuery.key));
                }
                if (permissionQuery.MenuId.HasValue)
                {
                    menuex = menuex.And(x => x.MenuId == permissionQuery.MenuId.Value);
                }
                var query = _permissionRepository.Where(menuex);
                permissionQuery.TotalCount = query.Count();
                var where = query.PageByIndex(permissionQuery.PageIndex, permissionQuery.PageSize);
                return ObjectMapper.Map<IEnumerable<Permission>, List<PermissionInfo>>(where);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 查询全部
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public List<PermissionInfo> GetPermissionList()
        {
            try
            {
                List<Permission> permissionInfoList = _permissionRepository.WhereIf(true, t => !t.IsDelete && t.Type != 0).ToList();
                return ObjectMapper.Map<IEnumerable<Permission>, List<PermissionInfo>>(permissionInfoList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetNextPermissionCode(string permissionCode)
        {
            try
            {
                if (!string.IsNullOrEmpty(permissionCode) && permissionCode.Length == 5)
                {
                    lock (locker)
                    {
                        string code = permissionCode;
                        if (_permissionRepository.WhereIf(true, p => p.Code != code).Any())
                        {
                            var permission = _permissionRepository.Where(zw => 1 == 1).OrderByDescending(p => p.Code).FirstOrDefault();
                            int codeNum = int.Parse(permission.Code) + 1;
                            permissionCode = Math.Round((codeNum / 10000m), 4).ToString("f4").Replace(".", "");
                        }
                    }
                    return permissionCode;
                }
                else
                {
                    permissionCode = "00001";
                    return await GetNextPermissionCode(permissionCode);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IQueryable<Permission> Entities()
        {
            return _permissionRepository.AsQueryable();
        }
    }
}

