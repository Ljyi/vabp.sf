﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VAbp.SF.System.Repository;

namespace VAbp.SF.System
{
    public class UserPermissionService : ServiceBase, IUserPermissionService
    {
        private IUserPermissionRepository _userPermissionRepository;
        IPermissionService permissionService;
        IMenuService menuService;
        public UserPermissionService(IUserPermissionRepository userPermissionRepository, IMenuService menuService, IPermissionService permissionService)
        {
            this._userPermissionRepository = userPermissionRepository;
            this.menuService = menuService;
            this.permissionService = permissionService;
        }

        public async Task BlukInsertAsync(List<UserPermission> userPermissions)
        {
            try
            {
                foreach (var item in userPermissions)
                {
                    await _userPermissionRepository.InsertAsync(item, true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task BlukDeleteAsync(List<int> userPermissionsIds)
        {
            try
            {
                await _userPermissionRepository.DeleteAsync(zw => userPermissionsIds.Contains(zw.Id), true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IQueryable<UserPermission> Entities()
        {
            return _userPermissionRepository.AsQueryable();
        }

        /// <summary>
        /// 根据用户Id查询菜单
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isAdmin"></param>
        /// <returns></returns>
        public List<MenuInfo> GetUserMenus(int userId, bool isAdmin = false)
        {
            List<MenuInfo> menuInfos = new List<MenuInfo>();
            List<Menu> menus = new List<Menu>();
            if (isAdmin)
            {
                menus = menuService.Entities().Where(zw => !zw.IsDelete).ToList();
            }
            else
            {
                var userPermissions = _userPermissionRepository.WhereIf(true, zw => !zw.IsDelete && zw.UserId == userId).ToList();
                List<int> PermissionId = userPermissions.Select(zw => zw.PermissionId).ToList();
                List<int> menuIds = permissionService.Entities().Where(zw => PermissionId.Contains(zw.Id)).Select(zw => zw.MenuId).ToList();
                // List<int> menuIds = userPermissions.Select(zw => zw.Permission.MenuId).Distinct().ToList();
                menus = menuService.Entities().Where(zw => !zw.IsDelete && menuIds.Contains(zw.Id)).ToList();
            }
            menuInfos = ObjectMapper.Map<IEnumerable<System.Menu>, List<MenuInfo>>(menus);
            return menuInfos;
        }
    }
}
