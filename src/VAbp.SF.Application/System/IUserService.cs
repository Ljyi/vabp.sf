﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VAbp.SF.Base;
using VAbp.SF.System.Query;

namespace VAbp.SF.System
{
    public interface IUserService
    {
        /// <summary>
        /// 创建用户
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<bool> AddUser(AddUserDto addUserDto);
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="editUserDto"></param>
        /// <returns></returns>
        Task<bool> EditUser(ModifyUserDto editUserDto);
        /// <summary>
        /// 更新用户
        /// </summary>
        /// <param name="updateUser"></param>
        /// <returns></returns>
        Task<bool> UpdateUser(UpdateUserDto updateUser);
        /// <summary>
        /// 根据Id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<UserInfo> GetUserById(int id);
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="account">账号</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        Task<UserInfo> Login(string account, string password);
        /// <summary>
        /// 查询全部
        /// </summary>
        /// <param name="userQuery"></param>
        /// <returns></returns>

        List<UserInfo> GetUserGrid(UserQuery userQuery);
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> DeleteByUserIdAsync(int id);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IQueryable<User> Entities();
    }
}
