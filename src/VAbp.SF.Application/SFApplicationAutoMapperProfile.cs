﻿using AutoMapper;
using System.Collections.Generic;
using VAbp.SF.System;

namespace VAbp.SF
{
    public class SFApplicationAutoMapperProfile : Profile
    {
        public SFApplicationAutoMapperProfile()
        {
            CreateMap<AddUserDto, User>();
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
            CreateMap<User, UserInfo>();
            CreateMap<Menu, MenuInfo>();
            CreateMap<AddMenuDto, Menu>();
            CreateMap<MenuIcon, MenuIconDto>();
            CreateMap<MenuIcon, MenuIconInfo>();
            CreateMap<Permission, PermissionInfo>();
            CreateMap<AddPermissionDto, Permission>();
            CreateMap<Order, OrderInfo>();
            CreateMap<Channel, ChannelInfo>();
            CreateMap<AddChannelDto, Channel>();
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
