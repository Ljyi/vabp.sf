﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VAbp.SF.System
{
    /// <summary>
    /// Icon
    /// </summary>
    [Table("MenuIcon")]
    public class MenuIcon : BaseModel
    {
        /// <summary>
        /// Code
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Code { get; set; }
        /// <summary>
        /// 尺寸
        /// </summary>
        [MaxLength(50)]
        public string Size { get; set; }
        /// <summary>
        /// 颜色
        /// </summary>
        [MaxLength(50)]
        public string Color { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
    }
}
