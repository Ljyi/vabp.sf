﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VAbp.SF.System
{
    [Table("Channel")]
    public class Channel : BaseModel
    {
        /// <summary>
        /// 渠道编码
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string ChannelCode { get; set; }
        /// <summary>
        /// 渠道名称
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string ChannelName { get; set; }
        /// <summary>
        /// 父节点Id
        /// </summary>
        [Required]
        public int ParentId { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 是否展示
        /// </summary>
        public bool Enable { get; set; }
    }
}
