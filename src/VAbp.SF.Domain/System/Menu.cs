﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VAbp.SF.System
{
    [Table("Menu")]
    public class Menu : BaseModel
    {
        /// <summary>
        /// 菜单编码
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string MenuCode { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string MenuName { get; set; }
        /// <summary>
        /// <summary>
        /// Url
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Url { get; set; }
        /// <summary>
        /// 页面别名
        /// </summary>
        [MaxLength(200)]
        public string Alias { get; set; }
        /// 菜单级别
        /// </summary>
        [Required]
        public int MenuLevel { get; set; }
        /// <summary>
        /// 父节点Id
        /// </summary>
        [Required]
        public int ParentId { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// Icon
        /// </summary>
        [MaxLength(100)]
        public string Icon { get; set; }
        /// <summary>
        /// 描述信息
        /// </summary>
        [MaxLength(200)]
        public string Description { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 前端组件(.vue)
        /// </summary>
        [MaxLength(200)]
        public string Component { get; set; }
        /// <summary>
        /// 是否隐藏
        /// </summary>
        public bool HideInMenu { get; set; }
        /// <summary>
        /// 是否展示
        /// </summary>
        public bool IsShow { get; set; }
        /// <summary>
        /// 缓存
        /// </summary>
        public bool NotCache { get; set; }
        /// <summary>
        /// 权限
        /// </summary>
        public virtual ICollection<Permission> Permissions { get; set; }
    }
}
