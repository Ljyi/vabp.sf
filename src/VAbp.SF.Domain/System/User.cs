﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace VAbp.SF.System
{
    public class User : Entity<int>
    {
        /// <summary>
        /// 用户名称
        /// </summary>
        [Required]
        [StringLength(20)]
        public string UserName { get; set; }
        /// <summary>
        /// 登录账号
        /// </summary>
        [Required]
        [StringLength(20)]
        public string Account { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        [Required]
        [StringLength(20)]
        public string Password { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        [StringLength(50)]
        public string Email { get; set; }
        /// <summary>
        /// 头像地址
        /// </summary>
        [MaxLength(100)]
        public string HeadImg { get; set; }
        /// <summary>
        /// 是否锁定
        /// </summary>
        public bool IsLocked { get; set; }
        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool Forbidden { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [StringLength(10)]
        public string Status { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 是否是管理员
        /// </summary>
        public bool IsAdmin { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDelete { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        [StringLength(50)]
        public string CreateUser { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CredateTime { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        [StringLength(50)]
        public string UpdateUser { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    }
}
