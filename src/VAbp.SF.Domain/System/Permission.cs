﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VAbp.SF.System
{
    /// <summary>
    /// 菜单权限
    /// </summary>
    [Table("Permission")]
    public class Permission : BaseModel
    {
        /// <summary>
        /// 权限编码
        /// </summary>
        [Required]
        [MaxLength(10)]
        public string Code { get; set; }
        /// <summary>
        /// 菜单Id
        /// </summary>
        public int MenuId { get; set; }
        /// <summary>
        /// 菜单别名(与前端路由配置中的name值保持一致)
        /// </summary>
        public string MenuAlias { get; set; }
        /// <summary>
        /// 权限名称
        /// </summary>
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }
        /// <summary>
        /// 权限操作码
        /// </summary>
        [Required]
        public string ActionCode { get; set; }
        /// <summary>
        /// 图标(可选)
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 请求接口地址
        /// </summary>
        [MaxLength(200)]
        public string ActionUrl { get; set; }
        /// <summary>
        /// 描述信息
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 权限类型(0:菜单,1:按钮/操作/功能等)
        /// </summary>
        public int Type { get; set; }
        public virtual Menu Menu { get; set; }
    }
}
