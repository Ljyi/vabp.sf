﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VAbp.SF.System
{
    /// <summary>
    /// 用户权限
    /// </summary>
    [Table("UserPermission")]
    public class UserPermission : BaseModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        [Required]
        public int UserId { get; set; }
        /// <summary>
        /// 权限Id
        /// </summary>
        [Required]
        public int PermissionId { get; set; }
        /// <summary>
        /// 权限
        /// </summary>
        public virtual Permission Permission { get; set; }
    }
}
