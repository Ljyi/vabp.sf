﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Volo.Abp.Domain.Repositories;

namespace VAbp.SF.System.Repository
{
    public interface IOrderRepository : IRepository<Order, int>
    {
        public IQueryable<Order> GetOrders(string sql);
    }
}
