﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Repositories;

namespace VAbp.SF.System.Repository
{
    public interface IUserRepository : IRepository<User, int>
    {

    }
}
