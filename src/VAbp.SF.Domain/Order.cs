﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace VAbp.SF
{
    /// <summary>
    /// 订单
    /// </summary>
    public class Order : Entity<int>
    {
        public string OrderNo { get; set; }
        public int Channel { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        public string Remark { get; set; }
        public string Phone { get; set; }
        public int Status { get; set; }
        public bool IsDelete { get; set; }
        public string CreateUser { get; set; }
        public DateTime CredateTime { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 是否同步
        /// </summary>
        public bool IsSync { get; set; }
    }
}
