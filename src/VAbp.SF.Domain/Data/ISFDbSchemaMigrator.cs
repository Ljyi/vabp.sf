﻿using System.Threading.Tasks;

namespace VAbp.SF.Data
{
    public interface ISFDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
