﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace VAbp.SF.Data
{
    /* This is used if database provider does't define
     * ISFDbSchemaMigrator implementation.
     */
    public class NullSFDbSchemaMigrator : ISFDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}