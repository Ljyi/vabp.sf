﻿using Volo.Abp.Settings;

namespace VAbp.SF.Settings
{
    public class SFSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(SFSettings.MySetting1));
        }
    }
}
