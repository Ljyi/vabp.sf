﻿using VAbp.SF.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace VAbp.SF.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(SFEntityFrameworkCoreDbMigrationsModule),
        typeof(SFApplicationContractsModule)
        )]
    public class SFDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
