﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VAbp.SF.BackgroundJobs.Model
{
    public class RequstOrder
    {
        public int C { get; set; }
        public int P { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Ip { get; set; }
    }
}
