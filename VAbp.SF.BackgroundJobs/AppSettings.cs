﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VAbp.SF.Base.Extensions;

namespace VAbp.SF.BackgroundJobs
{
    public class AppSettings
    {
        /// <summary>
        /// 配置文件的根节点
        /// </summary>
        private static readonly IConfigurationRoot _config;
        /// <summary>
        /// Constructor
        /// </summary>
        static AppSettings()
        {
            // 加载appsettings.json，并构建IConfigurationRoot
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", true, true);
            _config = builder.Build();
        }
        /// <summary>
        /// Hangfire
        /// </summary>
        public static class Hangfire
        {
            public static string Login => _config["Hangfire:Login"];

            public static string Password => _config["Hangfire:Password"];
        }
        public static class Requst
        {
            public static string Url => _config["Requst:Url"];

            public static int ProductId => int.Parse(_config["Requst:PId"]);

            public static int ChannelId => int.Parse(_config["Requst:CId"]);
            public static List<int> Channel => _config["Requst:Channel"].SplitDefaultToIntList();
            public static string Key => _config["Requst:Key"];
            public static string Secret => _config["Requst:Secret"];


        }
    }
}
