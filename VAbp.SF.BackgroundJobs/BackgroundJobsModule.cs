﻿using Hangfire;
using Hangfire.Dashboard.BasicAuthorization;
using Hangfire.SqlServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VAbp.SF.BackgroundJobs.Jobs;
using Volo.Abp;
using Volo.Abp.BackgroundJobs.Hangfire;
using Volo.Abp.Modularity;

namespace VAbp.SF.BackgroundJobs
{
    [DependsOn(typeof(AbpBackgroundJobsHangfireModule))]
    public class BackgroundJobsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var configuration = context.Services.GetConfiguration();
            var hostingEnvironment = context.Services.GetHostingEnvironment();
            ConfigureHangfire(context, configuration);
        }
        private void ConfigureHangfire(ServiceConfigurationContext context, IConfiguration configuration)
        {
            var tablePrefix = "hangfire";
            context.Services.AddHangfire(config =>
            {
                config.UseSqlServerStorage(configuration.GetConnectionString("Default"), new SqlServerStorageOptions
                {
                    SchemaName = tablePrefix
                });
                //config.UseConsole()
                //.SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                //.UseSimpleAssemblyNameTypeSerializer()
                //.UseRecommendedSerializerSettings();
            });
            //context.Services.AddHangfireConsoleExtensions();
            //context.Services.AddTransient<OrderServcieJob>();
            // context.Services.AddTransient<IBackgroundJob, OrderServcieJob>();
            //context.Services.AddHangfire(config =>
            //{
            //    var tablePrefix = MeowvBlogConsts.DbTablePrefix + "hangfire";

            //    switch (AppSettings.EnableDb)
            //    {
            //        case "MySql":
            //            config.UseStorage(
            //                new MySqlStorage(AppSettings.ConnectionStrings,
            //                new MySqlStorageOptions
            //                {
            //                    TablePrefix = tablePrefix
            //                }));
            //            break;

            //        case "Sqlite":
            //            config.UseSQLiteStorage(AppSettings.ConnectionStrings, new SQLiteStorageOptions
            //            {
            //                SchemaName = tablePrefix
            //            });
            //            break;

            //        case "SqlServer":
            //            config.UseSqlServerStorage(AppSettings.ConnectionStrings, new SqlServerStorageOptions
            //            {
            //                SchemaName = tablePrefix
            //            });
            //            break;

            //        case "PostgreSql":
            //            config.UsePostgreSqlStorage(AppSettings.ConnectionStrings, new PostgreSqlStorageOptions
            //            {
            //                SchemaName = tablePrefix
            //            });
            //            break;
            //    }
            //});
        }
        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();
            app.UseHangfireServer();
            app.UseHangfireDashboard(options: new DashboardOptions
            {
                Authorization = new[]
                {
                    new BasicAuthAuthorizationFilter(new BasicAuthAuthorizationFilterOptions
                    {
                        RequireSsl = false,
                        SslRedirect = false,
                        LoginCaseSensitive = true,
                        Users = new []
                        {
                            new BasicAuthAuthorizationUser
                            {
                               Login = AppSettings.Hangfire.Login,
                               PasswordClear =  AppSettings.Hangfire.Password
                            }
                        }
                    })
                },
                DashboardTitle = "任务调度中心"
            });
            var service = context.ServiceProvider;
            service.OrderJob();
        }
    }
}
