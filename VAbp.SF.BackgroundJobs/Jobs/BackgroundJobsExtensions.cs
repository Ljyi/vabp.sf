﻿using Hangfire;
using System;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Text;
using Hangfire.Server;

namespace VAbp.SF.BackgroundJobs.Jobs
{
    public static class BackgroundJobsExtensions
    {
        /// <summary>
        /// 订单同步
        /// </summary>
        /// <param name="service"></param>
        public static void OrderJob(this IServiceProvider service)
        {
            var job = service.GetService<OrderServcieJob>();
            RecurringJob.AddOrUpdate("订单同步", () => job.ExecuteAsync(), CronExtensions.Minute(5));
        }
    }
}
