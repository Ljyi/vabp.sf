﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VAbp.SF.BackgroundJobs.Model;
using VAbp.SF.Base;
using VAbp.SF.System.Repository;

namespace VAbp.SF.BackgroundJobs.Jobs
{
    public class OrderServcieJob : IBackgroundJob
    {
        private readonly IOrderRepository _orderRepository;
        //   PerformContext _context;
        public OrderServcieJob(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task ExecuteAsync()
        {
            try
            {
                List<int> channelIds = AppSettings.Requst.Channel;
                LoggerHelper.WriteToFile("定时任务");
                List<Order> orderList = await _orderRepository.GetListAsync();
                orderList = orderList.Where(zw => !zw.IsDelete && !zw.IsSync && channelIds.Contains(zw.Channel)).ToList();
                if (orderList.Count > 0)
                {
                    foreach (var item in orderList)
                    {
                        try
                        {
                            RequstOrder requstOrder = new RequstOrder()
                            {
                                C = AppSettings.Requst.ChannelId,
                                P = AppSettings.Requst.ProductId,
                                Name = item.UserName,
                                Mobile = item.Phone,
                                Address = item.Address,
                                Ip = "192.168.1.99"
                            };
                            string dataJson = JsonConvert.SerializeObject(requstOrder);
                            LoggerHelper.WriteToFile("请求数据：" + dataJson);
                            string result = HttpRequestHelp.Post(AppSettings.Requst.Url, dataJson.ToLower(), AppSettings.Requst.Key, AppSettings.Requst.Secret);
                            RequstResultModel requstResultModel = JsonConvert.DeserializeObject<RequstResultModel>(result);
                            if (requstResultModel.code == 0)
                            {
                                item.IsSync = true;
                                await _orderRepository.UpdateAsync(item, true);
                                LoggerHelper.WriteToFile("渠道：" + item.Channel + "同步成功");
                            }
                            else
                            {
                                LoggerHelper.WriteToFile("渠道：" + item.Channel + "同步失败，原因：" + requstResultModel.msg);
                            }
                        }
                        catch (Exception ex)
                        {
                            LoggerHelper.WriteToFile("请求失败", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.WriteToFile("请求失败", ex);
                throw ex;
            }
        }

    }
}