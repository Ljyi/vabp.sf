﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace VAbp.SF.HttpApi.Client.ConsoleTestApp
{
    [DependsOn(
        typeof(SFHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class SFConsoleApiClientModule : AbpModule
    {
        
    }
}
