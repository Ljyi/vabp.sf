﻿using VAbp.SF.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace VAbp.SF
{
    [DependsOn(
        typeof(SFEntityFrameworkCoreTestModule)
        )]
    public class SFDomainTestModule : AbpModule
    {

    }
}