﻿using Volo.Abp.Modularity;

namespace VAbp.SF
{
    [DependsOn(
        typeof(SFApplicationModule),
        typeof(SFDomainTestModule)
        )]
    public class SFApplicationTestModule : AbpModule
    {

    }
}